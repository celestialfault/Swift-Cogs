from contextlib import suppress

import discord
from red_icu import Humanize, cog_i18n
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.utils.chat_formatting import bold

from polls.poll import Poll, _

required_perms = discord.Permissions(send_messages=True, add_reactions=True, embed_links=True)


@cog_i18n(_)
class Polls(commands.Cog):
    """Create fancy persistant reaction polls"""

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot

    @commands.group()
    @commands.guild_only()
    async def poll(self, ctx: commands.Context):
        """Create and manage polls"""

    @poll.command(usage="<channel> <poll name>/<option>/<option>/...", aliases=["new"])
    @commands.bot_has_permissions(embed_links=True, add_reactions=True)
    async def create(self, ctx: commands.Context, channel: discord.TextChannel, *, options: str):
        """Create a new poll

        The poll name and options must be seperated with a slash (`/`) character.
        A single poll may only have 6 total options.

        **Example usage:**
        > `[p]poll create #channel-name Poll name/Option one/Option two/Option 3/...`
        """
        if not channel.permissions_for(ctx.author).send_messages:
            await ctx.send(
                _(
                    "Cannot create poll; you don't have permissions to send messages"
                    " in that channel"
                )
            )
            return
        cperms = channel.permissions_for(ctx.me)
        if not ctx.me.guild_permissions.administrator and (
            missing := required_perms.value & ~cperms.value
        ):
            raise commands.BotMissingPermissions(missing=discord.Permissions(missing))

        options = options.split("/")
        if len(options) < 3:
            await ctx.send(_("A poll must have a name and at least two different options").format())
            return

        name = options.pop(0)
        await Poll.create(channel=channel, name=name, options=options, creator=ctx.author)

    @poll.command(aliases=["close"])
    async def end(self, ctx: commands.Context, poll_message: discord.Message):
        """End an ongoing poll

        You must be the person who originally created the poll or an
        administrator to end polls.

        You can either use the poll's message ID or the message link
        to end it.
        """
        poll = await Poll.from_message(self.bot, message=poll_message)
        if not poll:
            await ctx.send(_("That message doesnt reference an ongoing poll.").format())
            return

        if ctx.author.id == poll.creator or ctx.author == ctx.guild.owner:
            pass
        elif await self.bot.is_owner(ctx.author) or await self.bot.is_admin(ctx.author):
            pass
        else:
            await ctx.send(
                _(
                    "You must either be an administrator or the person who originally created"
                    " this poll to end it."
                ).format()
            )
            return

        if poll.ended:
            await ctx.send(_("That poll has already ended!").format())
            return

        poll.ended = True
        await poll.save_and_update()
        winners = poll.winning
        content = (
            _(
                "Poll ended! The following option has won with {count, plural, one {# vote}"
                " other {# votes}}: {options}"
            )
            if len(winners) == 1
            else _(
                "Poll ended! The following options with {count, plural, one {# vote}"
                "other {# votes}} have won: {options}"
            )
        )
        await ctx.send(
            content.format(
                count=len(next(iter(winners)).votes),
                options=Humanize([bold(x.name) for x in winners]),
            ),
            allowed_mentions=discord.AllowedMentions(everyone=False, users=False, roles=False),
        )

    @commands.Cog.listener()
    async def on_raw_reaction_add(self, payload: discord.RawReactionActionEvent):
        if payload.emoji.is_custom_emoji():
            return
        channel = self.bot.get_channel(payload.channel_id)
        guild = getattr(channel, "guild", None)
        if not guild:
            return
        if await self.bot.cog_disabled_in_guild(self, guild):
            return
        member = await self.bot.get_or_fetch_member(guild, payload.user_id)
        if member.bot:
            return
        emoji = str(payload.emoji)
        poll = await Poll.from_message(bot=self.bot, message_id=payload.message_id)
        if not poll or poll.ended:
            return
        if str(payload.emoji) not in poll.emojis:
            return
        index = int(emoji[0]) - 1
        option = poll.options[index]

        for o in poll.options:
            if o == option:
                continue
            if member.id in o.votes:
                o.votes.remove(member.id)
                if channel.permissions_for(guild.get_member(self.bot.user.id)).manage_messages:
                    with suppress(discord.HTTPException):
                        await poll._message.remove_reaction(  # pylint:disable=protected-access
                            emoji=poll.emojis[poll.options.index(o)], member=member
                        )

        if member.id in option.votes:
            return
        option.votes.append(member.id)
        await poll.save_and_update()

    @commands.Cog.listener()
    async def on_raw_reaction_remove(self, payload: discord.RawReactionActionEvent):
        if payload.emoji.is_custom_emoji():
            return
        channel = self.bot.get_channel(payload.channel_id)
        guild = getattr(channel, "guild", None)
        if not guild:
            return
        if await self.bot.cog_disabled_in_guild(self, guild):
            return
        member = await self.bot.get_or_fetch_member(guild, payload.user_id)
        if member.bot:
            return
        emoji = str(payload.emoji)
        poll = await Poll.from_message(bot=self.bot, message_id=payload.message_id)
        if not poll or poll.ended:
            return
        if str(payload.emoji) not in poll.emojis:
            return
        index = int(emoji[0]) - 1
        option = poll.options[index]
        if member.id not in option.votes:
            return
        option.votes.remove(member.id)
        await poll.save_and_update()
