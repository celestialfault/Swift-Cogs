# Table of Contents

1. [Introduction](#introduction)
2. [Reporting issues](#reporting-issues)
3. [Setting up a dev environment](#setting-up-a-dev-environment)
   1. [Development guidelines](#development-guidelines)
5. [Translating](#translating)

# Introduction

Hi! Thanks for taking the time to contribute :purple_heart:

This document lays out a general set of guidelines *(not rules!)* to contributing to this repository.
Use your best judgement, and feel free to propose any changes to this document in a merge request.

# Reporting issues

If you're reporting a bug, these are usually the things I ask for in an issue:

- The output of `[p]debuginfo`
- How the issue happened
- What you expected to happen, and what actually happened
- How to reproduce the issue
- The relevant stacktrace, if applicable - please wrap these in code blocks!

# Setting up a dev environment

This repo is significantly more complex to setup a development environment for, almost entirely
due to the fact that it uses a shared library in all of the cogs here.

If you want to setup a development environment (or even just use repos outside of Downloader),
the following shell commands should get you setup:

```sh
# The following assumes that you've already setup a Python virtual env and have activated it.

# If you're intending on contributing, you should consider using the development branch of Red instead.
# Otherwise, if you just want to run these cogs without using Downloader, the latest release version will
# work just fine.
$ pip install red-discordbot
$ git clone https://gitlab.com/odinair/Swift-Cogs.git

# Setup cog dependencies
$ cd Swift-Cogs
$ pip install -r .requirements/dev.txt

# now you can add this repository with [p]addpath like you would with any other
# manually cloned repository.
```

## Development guidelines

**Please do the following:**

- Your code should be [Black](https://github.com/ambv/black) formatted, with a max line length of
  100 characters, with no underscore normalization (the relevant flags for this are `-l 100`)
- If you're adding a new major feature to a cog, open an issue explaining why you're adding it,
  and how it'd be useful to everyone, outside of your personal use case.
- Ensure your changes work on at least Windows, Mac OSX, and Linux

**Avoid the following, if at all possible:**

- Using features *only* present in the Red development git branch without suitable workarounds for stable releases
- Excessive code duplication
- `*` imports (`__init__.py` files are exempt from this for the purposes of namespace imports)
- Use of any blocking functions, methods, and/or libraries

# Translating

It'd be recommended to hold off on translating these cogs for the time being. (if you're adventurous,
it shouldn't be *too* hard to figure out how to translate these cogs, though.)

Most of these cogs are undergoing complete reworks to bring them up to my personal quality standards,
and along with that update them to (roughly) use the standards that Red has already established
for how translations are handled.

<!--
### Before you start...

Translations in this repository are a little different from what you may expect, as these cogs don't
use Red's translator, but instead use a completely different system for translations.

This allows for much greater control with how strings are translated, and also properly
implements full internationalization support (unlike Red's translator, which requires the use
of hard-coded `if` checks for plurals, which don't translate correctly for languages that
don't use English pluralization).

All the strings in this repository utilize ICU message formatting.

-----------

(this process will be made easier in the future; for now, you'll have to be slightly comfortable with using git and be willing to learn a bit. sorry!)

- Fork this repository by clicking `Fork` on the [project page](https://gitlab.com/odinair/Swift-Cogs)
- Clone your fork to your local machine, and locate the `locales` directory for the cog you'd like to translate
- Copy the `en-US` locale file and name the copy after the locale found in `[p]listlocales`, such as `es-ES` for Spanish;
  the locale name doesn't have to fully match, but can for example be just `es` instead of `es-ES`; however, the full
  locale name will take precedence in the event of conflicts, meaning if the bot's locale is set to `en-GB`, then it'll be
  preferred if it exists over `en` or `en-US`.
- Translate away!

Afterwards, open a [merge request](https://gitlab.com/odinair/Swift-Cogs/merge_requests) with your translation,
and I'll get back to you soon. Thanks again for helping with translations! :purple_heart:
-->
