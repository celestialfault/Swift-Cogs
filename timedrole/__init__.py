import json
from pathlib import Path

from redbot.core.bot import Red

with open(str(Path(__file__).parent / "info.json")) as f:
    __red_end_user_data_statement__ = json.load(f)["end_user_data_statement"]


def setup(bot: Red):
    from timedrole.timedrole import TimedRole

    bot.add_cog(TimedRole(bot))
