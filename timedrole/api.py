from datetime import datetime, timedelta
from typing import Optional, Union

import discord
from redbot.core.bot import Red
from redbot.core.utils import AsyncIter

from timedrole._shared import config

__all__ = ("TempRole",)


class TempRole:
    bot: Red = None

    def __init__(self, member: discord.Member, role: discord.Role, **kwargs):
        self.member = member
        self.role = role
        self.duration = timedelta(seconds=kwargs.pop("duration"))
        self.added_at = datetime.fromtimestamp(kwargs.pop("added_at"))
        self.added_by = self.guild.get_member(kwargs.get("added_by")) or kwargs.get("added_by")
        self.reason = kwargs.pop("reason", None)

    def __repr__(self):
        return f"<TempRole role={self.role!r} member={self.member!r} duration={self.duration!r}>"

    @property
    def mention(self):
        return self.role.mention

    @property
    def guild(self) -> discord.Guild:
        return self.member.guild

    @property
    def expires_at(self):
        """Get when the current role expires"""
        return self.added_at + self.duration

    @property
    def expired(self):
        """Check if the current role is considered expired"""
        return self.expires_at <= datetime.utcnow()

    @property
    def as_dict(self):
        return {
            "duration": self.duration.total_seconds(),
            "added_at": self.added_at.timestamp(),
            "added_by": getattr(self.added_by, "id", self.added_by),
            "reason": self.reason,
        }

    @classmethod
    async def all_roles(cls, *filters: Union[discord.Member, discord.Guild]):
        """Get all known timed roles

        This can be filtered down to a set of guilds and/or members.

        Passing a :class:`discord.Member` as a filter will implicitly add the
        member's guild as a filter as well.
        """
        filters = set(filters)
        guilds = {
            x.guild if isinstance(x, discord.Member) else x
            for x in filters
            if isinstance(x, (discord.Guild, discord.Member))
        }
        members = {x for x in filters if isinstance(x, discord.Member)}

        if guilds:
            all_roles = {}
            for guild in guilds:
                all_roles[guild.id] = await config.all_members(guild)
        else:
            all_roles = await config.all_members()

        for gid in all_roles:
            guild = cls.bot.get_guild(gid)
            if guild is None:
                continue
            data = all_roles[gid]

            async for mid in AsyncIter(data, steps=1000):
                member = guild.get_member(mid)
                if member is None:
                    continue
                if members and member not in members:
                    continue

                for rid, role_data in data[mid].items():
                    # None is used as a replacement for previously added but since expired roles
                    if role_data is None:
                        continue

                    role = discord.utils.get(guild.roles, id=int(rid))
                    if role is None:
                        continue
                    role = cls(member, role=role, **role_data)

                    if role:
                        yield role

    @classmethod
    async def get(cls, member: discord.Member, role: discord.Role) -> Optional["TempRole"]:
        """Retrieve an existing timed role for a member"""
        data = await config.member(member).get_raw(str(role.id), default=None)
        if data is None:
            return None
        return cls(member, role, **data)

    @classmethod
    async def create(
        cls,
        member: discord.Member,
        role: discord.Role,
        duration: timedelta,
        added_by: discord.Member,
        *,
        reason: str = None,
    ) -> "TempRole":
        """Create a timed role

        :class:`~timedrole.api.TempRole.apply_role` is not invoked for you,
        and must be manually done.

        Parameters
        -----------
        member: :class:`discord.Member`
            The member to assign the role to
        role: :class:`discord.Role`
            The role to assign to the given member
        duration: :class:`timedelta`
            How long to give ``role`` to ``member`` for
        added_by: :class:`discord.Member`
            The member who added the role to the user
        reason: :class:`str`
            An optional reason string. This is displayed in ``[p]timedrole list``
        """
        self = cls(
            member,
            role=role,
            duration=duration.total_seconds(),
            reason=reason,
            added_by=added_by.id,
            added_at=datetime.utcnow().timestamp(),
        )
        await self.save()
        return self

    async def save(self):
        """Save any changes made to the current role"""
        await config.member(self.member).set_raw(self.role.id, value=self.as_dict)

    async def delete(self):
        await config.member(self.member).set_raw(self.role.id, value=None)

    async def remove_role(self, *, reason: str = None):
        """Remove this timed role from the assigned member

        This action removes the current role from the member's data, and as such
        attempting to get this role via :class:`~timedrole.api.TempRole.get`
        will return :class:`None`.

        Parameters
        ----------
        reason: :class:`str`
            An optional reason string to display in the audit log
        """
        await self.delete()
        if self.role in self.member.roles:
            try:
                await self.member.remove_roles(self.role, reason=reason)
            except discord.HTTPException:
                pass

    async def apply_role(self, *, reason: str = None):
        """Apply this timed role to the assigned member

        Parameters
        ----------
        reason: :class:`str`
            An optional reason to display in the audit log
        """
        if self.role not in self.member.roles:
            try:
                await self.member.add_roles(self.role, reason=reason)
            except discord.HTTPException:
                pass
