.toml files in this directory will be read by `../.scripts/friendly_info.py`
and will generate an info.json file into the directory with the same name as
the .toml file.

.toml files starting with a `.` (i.e hidden files) will not result in a
info.json file being generated however.

Additionally, .globals.toml is a special file - all properties
added in it will be copied into all info.json files generated,
unless specifically excluded with an `__exclude__` property.
