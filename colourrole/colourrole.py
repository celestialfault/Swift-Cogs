from typing import Tuple, Literal

import discord
from redbot.core import Config, checks, commands
from redbot.core.bot import Red
from redbot.core.utils import AsyncIter
from redbot.core.utils.chat_formatting import error, info, escape

from swift_i18n.red import Translator

translate = Translator(__file__)


def tick(text: str) -> str:
    """Return `text` with a check mark emoji prepended"""
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


async def hierarchy_allows(
    bot: Red, mod: discord.Member, member: discord.Member, *, allow_disable: bool = True
) -> bool:
    if await bot.is_owner(mod):
        return True

    guild = mod.guild
    mod_cog = bot.get_cog("Mod")
    # noinspection PyUnresolvedReferences
    mod_config = (
        (mod_cog.settings if hasattr(mod_cog, "settings") else mod_cog.config) if mod_cog else None
    )
    if (
        allow_disable
        and mod_cog is not None
        and not await mod_config.guild(guild).respect_hierarchy()
    ):
        return True

    return member != guild.owner and (mod == guild.owner or mod.top_role > member.top_role)


def has_cr():
    async def predicate(ctx: commands.Context):
        guild: discord.Guild = ctx.guild
        bot: Red = ctx.bot
        if not guild:
            raise commands.NoPrivateMessage
        if await bot.is_mod(ctx.author):
            return True

        # noinspection PyTypeChecker
        cog: ColourRole = ctx.bot.get_cog(ColourRole.__name__)
        config: Config = cog.config
        if not ctx.guild.get_role(await config.member(ctx.author).role()):
            raise commands.CheckFailure(translate("no_cr"))
        return True

    return commands.check(predicate)


def cr_enabled():
    async def predicate(ctx: commands.Context):
        guild: discord.Guild = ctx.guild
        bot: Red = ctx.bot
        if not guild:
            raise commands.NoPrivateMessage
        # mod+ can always use colour roles regardless of settings
        if await bot.is_mod(ctx.author) or ctx.author.guild_permissions.manage_roles:
            return True

        # noinspection PyTypeChecker
        cog: ColourRole = ctx.bot.get_cog(ColourRole.__name__)
        config: Config = cog.config
        if not await config.guild(guild).enabled():
            rid = await config.member(ctx.author).role()
            role = ctx.guild.get_role(rid)
            if role is None:
                raise commands.CheckFailure(translate("not_allowed"))
        return True

    return commands.check(predicate)


@translate.cog("help.cog_class")
class ColourRole(commands.Cog):
    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=2143463442, force_registration=True)
        self.config.register_member(role=None)
        self.config.register_role(owned_by=None)
        self.config.register_guild(enabled=False)

    async def red_delete_data_for_user(
        self,
        *,
        requester: Literal["discord_deleted_user", "owner", "user", "user_strict"],
        user_id: int,
    ):
        if requester not in ("discord_deleted_user", "owner", "user_strict"):
            return

        async for gid, members in AsyncIter((await self.config.all_members()).items(), steps=100):
            if user_id in members:
                if role_id := members[user_id].get("role", None):
                    await self.config.role_from_id(role_id).clear()
                    guild = self.bot.get_guild(gid)
                    if (
                        guild
                        and guild.get_member(self.bot.user.id).guild_permissions.manage_roles
                        and (role := guild.get_role(role_id))
                    ):
                        # yes, of course, this may be undefined, let's ignore the 'and'.
                        # thanks pycharm, helpful as always.
                        # noinspection PyUnboundLocalVariable
                        await role.delete(reason="Role deleted due to user data deletion request")
                await self.config.member_from_ids(gid, user_id).clear()

    async def _get_role(
        self,
        member: discord.Member,
        *,
        name: str = None,
        colour: discord.Colour = discord.Colour.default(),
        create: bool = True,
    ) -> Tuple[discord.Role, bool]:
        guild: discord.Guild = member.guild
        role = guild.get_role(await self.config.member(member).role())
        if not role and create:
            role = await guild.create_role(
                name=name or translate("default_name"),
                colour=colour,
                reason=translate("audit_create", member=member, id=member.id),
                permissions=discord.Permissions.none(),
            )
            await member.add_roles(role)
            await self.config.member(member).role.set(role.id)
            await self.config.role(role).owned_by.set(member.id)
            return role, True
        return role, False

    @commands.group(aliases=["colorrole", "cr"])
    @commands.guild_only()
    @checks.bot_has_permissions(manage_roles=True)
    @translate.command("help.colourrole.root")
    async def colourrole(self, ctx: commands.Context):
        pass

    @colourrole.command(name="name")
    @commands.cooldown(2, 60, commands.BucketType.member)
    @cr_enabled()
    @translate.command("help.colourrole.name")
    async def cr_name(self, ctx: commands.Context, *, name: str):
        conflicts = [x for x in ctx.guild.roles if x.name.lower() == name.lower()]
        if conflicts and not await self.bot.is_admin(ctx.author):
            roles = {
                x.get("role")
                async for x in AsyncIter(
                    (await self.config.all_members(ctx.guild)).values(), steps=100
                )
            }
            if any([conflict.id not in roles for conflict in conflicts]):
                return await ctx.send(error(translate("name_conflict")))

        role, created = await self._get_role(ctx.author, name=name)
        if not created:
            await role.edit(
                name=name, reason=translate("audit_update", member=ctx.author, id=ctx.author.id)
            )
        await ctx.send(
            tick(translate("name_changed", name=escape(name, formatting=True, mass_mentions=True)))
        )

    @colourrole.command(name="colour", aliases=["color"])
    @commands.cooldown(2, 60, commands.BucketType.member)
    @cr_enabled()
    @translate.command("help.colourrole.colour")
    async def cr_colour(self, ctx: commands.Context, colour: discord.Colour):
        role, created = await self._get_role(ctx.author, colour=colour)
        if not created:
            await role.edit(
                colour=colour, reason=translate("audit_update", member=ctx.author, id=ctx.author.id)
            )
        await ctx.send(tick(translate("colour_changed", colour=colour)))

    @colourrole.command(name="remove")
    @has_cr()
    @translate.command("help.colourrole.remove")
    async def cr_remove(self, ctx: commands.Context, *, member: discord.Member = None):
        if member and member != ctx.author:
            if not (
                ctx.guild.owner == ctx.author
                or ctx.author.guild_permissions.manage_roles
                or await self.bot.is_mod(ctx.author)
            ):
                return await ctx.send(error("cannot_remove_other"))
            if not hierarchy_allows(bot=self.bot, member=member, mod=ctx.author):
                return await ctx.send(error("hierarchy_disallows"))

        member = member or ctx.author
        role, _ = await self._get_role(member, create=False)
        if not role:
            await ctx.send(error(translate("no_role", "self" if member == ctx.author else "other")))
            return
        await role.delete(reason=translate("audit_delete", member=ctx.author, id=ctx.author.id))
        await ctx.send(tick(translate("role_removed", "self" if member == ctx.author else "other")))

    @colourrole.command(name="whoowns", aliases=["who", "owner"])
    @checks.mod_or_permissions(manage_roles=True)
    @translate.command("help.colourrole.whoowns")
    async def cr_whoowns(self, ctx: commands.Context, *, role: discord.Role):
        owner = await self.config.role(role).owned_by()
        if not owner:
            await ctx.send(translate("role_not_known"))
            return
        member = ctx.guild.get_member(owner)
        await ctx.send(info(translate("role_owned_by", member=member, id=member.id)))

    @colourrole.command(name="toggle")
    @checks.admin_or_permissions(manage_roles=True)
    @translate.command("help.colourrole.toggle")
    async def cr_toggle(self, ctx: commands.Context, toggle: bool = None):
        val = (not await self.config.guild(ctx.guild).enabled()) if toggle is None else toggle
        await self.config.guild(ctx.guild).enabled.set(val)
        await ctx.send(tick(translate("enabled" if val else "disabled")))
