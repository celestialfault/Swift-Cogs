from datetime import timedelta
from typing import Optional

import discord
from redbot.core import Config, checks, commands, modlog
from redbot.core.bot import Red
from redbot.core.utils.chat_formatting import warning

from red_icu import Humanize, Translator, cog_i18n

TimeConverter = commands.get_timedelta_converter(minimum=timedelta(minutes=2), default_unit="hours")
_ = Translator("TimedMute", __file__)


def tick(text: str) -> str:
    return f"\N{WHITE HEAVY CHECK MARK} {text}"


async def hierarchy_allows(
    bot: Red, mod: discord.Member, member: discord.Member, *, allow_disable: bool = True
) -> bool:
    if await bot.is_owner(mod):
        return True

    guild = mod.guild
    mod_cog = bot.get_cog("Mod")
    # noinspection PyUnresolvedReferences
    mod_config = (
        (mod_cog.settings if hasattr(mod_cog, "settings") else mod_cog.config) if mod_cog else None
    )
    if (
        allow_disable
        and mod_cog is not None
        and not await mod_config.guild(guild).respect_hierarchy()
    ):
        return True

    return member != guild.owner and (mod == guild.owner or mod.top_role > member.top_role)


def cogs_loaded(*cogs):
    def predicate(ctx):
        missing = set(cogs) - set(ctx.bot.cogs.keys())
        if missing:
            raise commands.UserFeedbackCheckFailure(
                _(
                    "The following {count, plural, one {cog is} other {cogs are}}"
                    " not loaded: {cogs}"
                ).format(cogs=Humanize([f"`{x}`" for x in missing]), count=len(missing))
            )
        return True

    return commands.check(predicate)


@cog_i18n(_)
class TimedMute(commands.Cog):
    """Silence problematic users for a set amount of time"""

    OVERWRITE_PERMISSIONS = discord.PermissionOverwrite(
        speak=False, send_messages=False, add_reactions=False
    )

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=12903812, force_registration=True)
        self.config.register_guild(punished_role=None)
        self._cases_task = self.bot.loop.create_task(self._setup_cases())

    def cog_unload(self):
        self._cases_task.cancel()

    async def red_delete_data_for_user(self, **_):
        pass  # nothing to delete

    @staticmethod
    async def _setup_cases():
        try:
            await modlog.register_casetype(
                name="timedmute",
                default_setting=True,
                # as much as I've love to be able to translate this string, it can't really be
                # reasonably done, as case names aren't translated in core Red cogs,
                # and there's no real reasonable way to translate them.
                case_str="Timed Mute",
                image="\N{STOPWATCH}\N{SPEAKER WITH CANCELLATION STROKE}",
            )
        except RuntimeError:
            pass

    async def setup_overwrites(self, role: discord.Role, channel: discord.abc.GuildChannel):
        # noinspection PyUnresolvedReferences
        if not channel.permissions_for(channel.guild.me).manage_roles:
            return
        await channel.set_permissions(
            target=role,
            overwrite=self.OVERWRITE_PERMISSIONS,
            reason=_("Setting up muted role permissions"),
        )

    async def setup_role(self, guild: discord.Guild) -> discord.Role:
        role = await guild.create_role(name=_("Muted"), permissions=discord.Permissions.none())

        for channel in guild.channels:
            await self.setup_overwrites(role, channel)

        await self.config.guild(guild).punished_role.set(role.id)
        return role

    async def get_punished_role(self, guild: discord.Guild) -> Optional[discord.Role]:
        return discord.utils.get(guild.roles, id=await self.config.guild(guild).punished_role())

    @commands.command(aliases=["tempmute"])
    @commands.guild_only()
    @cogs_loaded("TimedRole")
    @checks.mod_or_permissions(manage_roles=True)
    @checks.bot_has_permissions(manage_roles=True)
    async def timedmute(
        self,
        ctx: commands.Context,
        member: discord.Member,
        duration: TimeConverter,
        *,
        reason: str = None,
    ):
        """Mute a user for a set amount of time

        Muted users will not be able to send messages, add new reactions to messages
        (they can still use existing reactions), or speak in voice channels.

        Examples for duration: `1h30m`, `3d`, `1mo`.
        Longer form values (such as `2 hours`) are also accepted, but must be wrapped
        in double quotes if they contain spaces. Minimum duration for a mute is 2 minutes.

        Mutes added with this command can be managed with `[p]timedrole`, and similarly
        are also expired by the same functionality as timed roles.
        """

        from timedrole.api import TempRole

        if not await hierarchy_allows(self.bot, mod=ctx.author, member=member):
            await ctx.send(
                warning(
                    _(
                        "This server's role hierarchy doesn't allow you to perform this action."
                    ).format()
                )
            )
            return

        role = await self.get_punished_role(ctx.guild)
        if role is None:
            tmp_msg = await ctx.send(
                _(
                    "Setting up this server's muted role... (this may take a while if"
                    " you have a lot of channels)"
                ).format()
            )
            async with ctx.typing():
                role = await self.setup_role(ctx.guild)
            await tmp_msg.delete()

        if role in member.roles:
            await ctx.send(
                warning(_("{member} is already muted.").format(member=member.mention)),
                allowed_mentions=discord.AllowedMentions(users=False),
            )
            return

        role = await TempRole.create(
            member, role, duration=duration, added_by=ctx.author, reason=reason
        )
        await role.apply_role(reason=reason)

        try:
            await modlog.create_case(
                bot=self.bot,
                guild=ctx.guild,
                user=member,
                moderator=ctx.author,
                reason=reason,
                until=role.expires_at,
                action_type="timedmute",
                created_at=role.added_at,
            )
        except RuntimeError:
            pass

        await ctx.send(
            tick(
                _("{member} is now muted for {duration}.").format(
                    member=member.mention, duration=Humanize(duration)
                )
            ),
            allowed_mentions=discord.AllowedMentions(users=False),
        )

    @commands.Cog.listener()
    async def on_guild_channel_create(self, channel: discord.abc.GuildChannel):
        # noinspection PyUnresolvedReferences
        guild: discord.Guild = channel.guild
        if await self.bot.cog_disabled_in_guild(self, guild):
            return
        if not channel.permissions_for(guild.me).manage_roles:
            return
        role = await self.get_punished_role(guild)
        if not role:
            return
        await self.setup_overwrites(role, channel)
