class NotEnoughCredits(Exception):
    pass


class AlreadyInHeist(Exception):
    pass


class HeistOnCooldown(Exception):
    pass
