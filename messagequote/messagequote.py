from __future__ import annotations

import logging
import re
import textwrap
from dataclasses import dataclass
from typing import Dict, List, Sequence, Set, Tuple, Union

import discord
from babel.lists import format_list
from red_icu import Translator, cog_i18n
from red_icu.util import LazyStr
from redbot.core import Config, checks, commands
from redbot.core.bot import Red
from redbot.core.i18n import get_babel_regional_format

config = Config.get_conf(
    cog_instance=None, cog_name="MessageQuote", identifier=50551996, force_registration=True
)
config.register_guild(respond_to_messages=False, ignore=[])
log = logging.getLogger("red.swift_cogs.messagequote")
_ = Translator("MessageQuote", __file__)
MESSAGE_URL = re.compile(
    r"https?://(?:(ptb|canary|www)\.)?discord(?:app)?\.com/channels/"
    r"(?:[0-9]{15,21}|@me)/(?P<channel_id>[0-9]{15,21})/(?P<message_id>[0-9]{15,21})/?"
)
_GUILDS: Dict[int, GuildCache] = {}


@dataclass(eq=False)
class GuildCache:
    __slots__ = ("guild", "data")

    guild: discord.Guild
    data: dict

    @classmethod
    async def load(cls, guild: discord.Guild) -> GuildCache:
        if guild.id not in _GUILDS:
            _GUILDS[guild.id] = cls(guild=guild, data=await config.guild(guild).all())
        return _GUILDS[guild.id]

    async def save(self):
        await config.guild(self.guild).set(self.data)

    @property
    def respond(self) -> bool:
        return self.data.get("respond_to_messages", False)

    @respond.setter
    def respond(self, value: bool):
        self.data["respond_to_messages"] = value

    @property
    def ignore(self) -> List[int]:
        if "ignore" not in self.data:
            self.data["ignore"] = []
        return self.data["ignore"]

    def quotable(self, message: discord.Message, member: discord.Member):
        channel: discord.TextChannel = message.channel
        if channel.id in self.ignore or channel.category and channel.category.id in self.ignore:
            if not member.guild_permissions.administrator:
                return False
            log.debug("%r is ignored, but %r has administrator permissions", channel, member)
        return bool(channel.permissions_for(member).read_message_history)


@cog_i18n(_)
class MessageQuote(commands.Cog):
    """Quote a message with its message link or ID

    [botname] can also be configured to respond to messages with message links with the quoted
    message (only if configured by server admins - `[p]help messagequote respond`).
    """

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot

    async def load_messages(
        self, channel: discord.TextChannel, messages: Sequence[str]
    ) -> Tuple[List[discord.Message], Dict[int, LazyStr]]:
        pass

    @commands.Cog.listener()
    async def on_message_without_command(self, message: discord.Message):
        cache = await GuildCache.load(message.guild)
        if (
            # Ignore bots
            message.author.bot
            or message.author == self.bot.user
            # Ignore DM contexts
            or not message.guild
            # Make sure we have send message and embed links permissions
            or not message.channel.permissions_for(message.guild.me).send_messages
            or not message.channel.permissions_for(message.guild.me).embed_links
            # And that the guild in question wants this behaviour
            or await self.bot.cog_disabled_in_guild(self, message.guild)
            or not cache.respond
        ):
            return

        quoted: Set[int] = set()
        for match in MESSAGE_URL.finditer(message.content):
            if len(quoted) >= 3:
                # Only allow 3 quotes per message
                return
            log.debug("Found match in message: %r", match)
            channel, msg_id = int(match.group("channel_id")), int(match.group("message_id"))
            if msg_id in quoted:
                log.debug("Already quoted, ignoring")
                continue
            channel: Union[int, discord.TextChannel, None] = self.bot.get_channel(channel)
            if not channel or channel.guild != message.guild:
                continue

            try:
                # This is in the try block to ensure that this is still counted toward
                # the 3 quote limit.
                if (
                    not channel.permissions_for(message.guild.me).read_message_history
                    or not channel.permissions_for(message.author).read_message_history
                ):
                    log.debug("Message author or myself don't have history permissions, ignoring")
                    continue

                if cached := discord.utils.get(self.bot.cached_messages, id=msg_id):
                    log.debug("Message is in the bot cache")
                    msg = cached
                else:
                    log.debug("Message is not cached, fetching from Discord")
                    # noinspection PyProtectedMember
                    msg = await channel.fetch_message(msg_id)

                if not cache.quotable(msg, message.author):
                    log.debug("Can't quote message, skipping")
                    continue

                await message.channel.send(
                    content=_("Quoted by {user}").format(user=message.author.mention),
                    allowed_mentions=discord.AllowedMentions(users=False),
                    embed=self._message_embed(msg),
                )
            except (discord.NotFound, discord.Forbidden):
                log.debug("Failed to retrieve message")
                continue
            finally:
                quoted.add(msg_id)

    @staticmethod
    def _message_embed(message: discord.Message) -> discord.Embed:
        content = (
            textwrap.shorten(message.content, 2000, placeholder="\N{HORIZONTAL ELLIPSIS}")
            if message.content
            else discord.Embed.Empty
        )
        embed = discord.Embed(
            colour=message.author.colour, description=content, timestamp=message.created_at
        ).set_author(
            name=message.author.display_name,
            icon_url=message.author.avatar_url_as(format="png"),
        )

        if message.attachments:
            # noinspection PyUnboundLocalVariable
            if (
                len(message.attachments) == 1
                and (attach := message.attachments[0]).width
                and attach.height  # pylint:disable=used-before-assignment
            ):
                embed.set_image(url=attach.url)
            else:
                attachments = []
                for attach in message.attachments:
                    if attach.is_spoiler():
                        attachments.append(f"||[`{attach.filename}`]({attach.url})||")
                    else:
                        attachments.append(f"[`{attach.filename}`]({attach.url})")
                embed.add_field(
                    name=_("{count, plural, one {Attachment} other {Attachments}}").format(
                        count=len(attachments)
                    ),
                    value=format_list(attachments, locale=get_babel_regional_format()),
                )

        jump = _("\N{BLACK RIGHTWARDS ARROW} [Jump to message]({jump})").format(
            jump=message.jump_url
        )
        if message.content == "":
            embed.description = jump
        else:
            embed.add_field(name="\N{ZERO WIDTH JOINER}", value=jump)
        return embed

    @commands.group(aliases=["mq"], usage="<message>...", invoke_without_command=True)
    @commands.guild_only()
    @commands.bot_has_permissions(read_message_history=True, embed_links=True)
    async def messagequote(self, ctx: commands.GuildContext, *messages):
        """Retrieve a message with its message link or ID

        Only 5 messages may be retrieved per command invocation.
        """
        if not messages or len(messages) > 5:
            await ctx.send_help()
            return

        converter = commands.PartialMessageConverter()
        messages = [await converter.convert(ctx, x) for x in messages]

        for msg in messages:
            channel = msg.channel
            if not (await GuildCache.load(ctx.guild)).quotable(msg, ctx.author):
                # While this does allow users to know that a message with the given ID
                # does exist, just knowing that a message with a given ID exists isn't
                # really all too useful.
                await ctx.send(
                    _(
                        "Cannot retrieve message `{id}` as you either don't have permission to"
                        " read the message history of {channel}, or otherwise don't have"
                        " permission to quote that message."
                    ).format(channel=channel.mention, id=msg.id)
                )
                continue
            await ctx.send(
                content=_("Message `{id}` sent in {channel}:").format(
                    id=msg.id, channel=channel.mention
                ),
                embed=self._message_embed(await msg.fetch()),
            )

    @messagequote.command(name="respond", usage="[(yes|no)]")
    @checks.admin_or_permissions(manage_channels=True)
    async def mq_respond(self, ctx: commands.GuildContext, toggle: bool = None):
        """Toggle if [botname] should quote message links in messages

        Only the first 3 unique message links per message will be quoted.
        (message links that don't resolve to a valid message are also counted
        toward this limit)"""
        cache = await GuildCache.load(ctx.guild)
        cache.respond = toggle if toggle is not None else not cache.respond
        await cache.save()
        await ctx.send(
            (
                _("\N{WHITE HEAVY CHECK MARK} I will now quote linked messages.")
                if cache.respond
                else _("\N{WHITE HEAVY CHECK MARK} I will no longer quote linked messages.")
            ).format()
        )

    @messagequote.group(name="ignore")
    @checks.admin_or_permissions(manage_channels=True)
    async def mq_ignore(self, ctx: commands.Context):
        """Prevent quoting messages in certain channels

        This applies to both linked messages (via `[p]messagequote respond`) as well
        as when quoting using `[p]messagequote` itself.

        Members must already have Read Message History permissions in channels
        for messages that they're quoting; this instead prevents members with
        such permissions from quoting messages they can otherwise read.

        Members with the Administrator permission bypass this restriction.

        Category channels are also accepted and will implicitly ignore all channels
        contained in it, regardless of if they're also marked to be ignored or not.
        """

    @mq_ignore.command(name="add")
    async def mq_ignore_add(
        self, ctx: commands.Context, channel: Union[discord.TextChannel, discord.CategoryChannel]
    ):
        """Prevent quoting messages from a certain channel"""
        cache = await GuildCache.load(ctx.guild)
        if channel.id in cache.ignore:
            await ctx.send(_("That channel is already ignored.").format())
            return
        cache.ignore.append(channel.id)
        await cache.save()
        await ctx.send(_("Okay; members can no longer quote messages from that channel.").format())

    @mq_ignore.command(name="remove")
    async def mq_ignore_remove(
        self, ctx: commands.Context, channel: Union[discord.TextChannel, discord.CategoryChannel]
    ):
        """Allow quoting messages from a certain channel again"""
        cache = await GuildCache.load(ctx.guild)
        if channel.id not in cache.ignore:
            await ctx.send(_("That channel is not currently ignored.").format())
            return
        cache.ignore.remove(channel.id)
        await cache.save()
        await ctx.send(_("Okay; members can now quote messages from that channel again.").format())
