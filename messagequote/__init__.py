from redbot.core.bot import Red

import json
from pathlib import Path

with open(str(Path(__file__).parent / "info.json")) as f:
    __red_end_user_data_statement__ = json.load(f)["end_user_data_statement"]


def setup(bot: Red):
    from messagequote.messagequote import MessageQuote

    bot.add_cog(MessageQuote(bot))
