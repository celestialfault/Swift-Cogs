from typing import Optional

import discord
from redbot.core import Config, checks
from redbot.core.bot import Red
from redbot.core.utils.caching import LRUDict
from redbot.core.utils.chat_formatting import error

from misctools import commands
from misctools.shared import _
from misctools.toolset import Toolset, register_defaults
from red_icu import cog_i18n


@cog_i18n(_)
class VCRole(Toolset, name=_("VC Role")):
    """Give members a role when they join any voice channel"""

    def __init__(self, bot: Red):
        super().__init__(bot)
        register_defaults(Config.GUILD, role=None)
        self.cache = LRUDict(size=5_000)
        self.bot.add_listener(self.on_voice_state_update)

    def toolset_cleanup(self):
        self.bot.remove_listener(self.on_voice_state_update)

    @commands.command()
    @commands.guild_only()
    @checks.admin_or_permissions(manage_roles=True)
    @checks.bot_has_permissions(manage_roles=True)
    async def vcrole(self, ctx: commands.Context, *, role: Optional[discord.Role] = None):
        """Set the role to give when members join a voice channel"""
        if role >= ctx.me.top_role:
            await ctx.send(error(_("That role is above my topmost role").format()))
            return
        if ctx.guild.id in self.cache:
            del self.cache[ctx.guild.id]
        await self.config.guild(ctx.guild).role.set(getattr(role, "id", None))
        if role:
            await ctx.send(
                _("Okay; I will now give members {role} when they join a voice channel.").format(
                    role=role.mention
                ),
                allowed_mentions=discord.AllowedMentions(roles=False),
            )
        else:
            await ctx.send(
                _(
                    "Okay; I will no longer give members a role when they join a voice channel."
                ).format()
            )

    async def role(self, guild: discord.Guild) -> Optional[discord.Role]:
        if guild.id in self.cache:
            return self.cache[guild.id]

        role = await self.config.guild(guild).role()
        if isinstance(role, int):
            role = guild.get_role(role)
        self.cache[guild.id] = role
        return role

    async def on_voice_state_update(
        self, member: discord.Member, before: discord.VoiceState, after: discord.VoiceState
    ):
        if (
            not member.guild
            or not member.guild.get_member(self.bot.user.id).guild_permissions.manage_roles
        ):
            return
        if await self.bot.cog_disabled_in_guild_raw("MiscTools", member.guild.id):
            return

        role = await self.role(member.guild)
        if not role:
            return

        if before.channel and not after.channel and role in member.roles:
            await member.remove_roles(role)

        if not before.channel and after.channel and role not in member.roles:
            await member.add_roles(role)
