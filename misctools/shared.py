import argparse
import logging

from redbot.core import Config, commands

from red_icu import Translator

_ = Translator("MiscTools", __file__)
log = logging.getLogger("red.misctools")

try:
    config = Config.get_conf(
        None, cog_name="MiscTools", identifier=421412412, force_registration=True
    )
    config.register_global(toolsets=[])
except RuntimeError:
    config = None


class Arguments(argparse.ArgumentParser):
    def error(self, message):
        raise commands.BadArgument(message)
