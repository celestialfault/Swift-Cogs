from redbot.core import checks, commands
from redbot.core.bot import Red
from redbot.core.utils.chat_formatting import inline, pagify
from redbot.vendored.discord.ext.menus import MenuPages

from misctools.load_logic import (
    AlreadyLoaded,
    InternalLoadError,
    LoadLogic,
    NotLoaded,
    UnknownToolset,
)
from misctools.shared import _, config, log
from misctools.tools import toolsets
from misctools.toolset_list import ToolsetListPageSource
from red_icu import Humanize, cog_i18n


@cog_i18n(_)
class MiscTools(commands.Cog):
    """A varied collection of small but helpful tools"""

    def __init__(self, bot: Red):
        super().__init__()
        self.bot = bot
        self.loader = LoadLogic(bot=bot, cog=self)
        self.loader.update_commands()

    def cog_unload(self):
        self.loader.cog_unload()

    async def red_delete_data_for_user(self, **_):
        pass  # nothing to delete

    @commands.group()
    @checks.is_owner()
    async def misctools(self, ctx: commands.Context):
        """Manage loaded MiscTools toolsets"""

    @misctools.command(name="list")
    @checks.bot_has_permissions(embed_links=True)
    async def misctools_list(self, ctx: commands.Context, show_hidden: bool = False):
        """List all available toolsets"""
        await MenuPages(
            source=ToolsetListPageSource(
                [
                    x
                    for x in toolsets
                    if not x.__hidden__ or show_hidden or self.loader.is_loaded(x)
                ],
                self.loader.loaded,
                per_page=1,
            ),
            clear_reactions_after=True,
        ).start(ctx)

    @misctools.command(name="load", aliases=["enable", "add"], usage="<toolsets...>")
    async def misctools_load(self, ctx: commands.Context, *toolsets_: str):
        """Load one or more toolsets"""
        if not toolsets_:
            await ctx.send_help()
            return

        loaded = []
        output = []

        for toolset in toolsets_:
            try:
                await self.loader.load(toolset)
            except AlreadyLoaded:
                output.append(_("`{toolset}` is already loaded").format(toolset=toolset))
            except UnknownToolset:
                output.append(
                    _("No such toolset with the name `{toolset}` could be found").format(
                        toolset=toolset
                    )
                )
            except InternalLoadError as e:
                output.append(str(e.translated))
            except Exception as e:  # pylint:disable=broad-except
                log.exception("Failed to load toolset %r", toolset.lower(), exc_info=e)
                output.append(
                    _("Failed to load `{toolset}`; check your logs for more information").format(
                        toolset=toolset
                    )
                )
            else:
                async with config.toolsets() as toolsets__:
                    ts = self.loader.find(toolset).__name__
                    if ts not in toolsets__:
                        toolsets__.append(ts)
                loaded.append(toolset)

        if loaded:
            output.append(
                _(
                    "Loaded {count, plural, one {{count} toolset} other {{count} toolsets}}: {sets}"
                ).format(count=len(loaded), sets=Humanize([inline(x) for x in loaded]))
            )

        for page in pagify("\n".join(output)):
            await ctx.send(page)

    @misctools.command(name="unload", aliases=["disable", "remove"], usage="<toolsets...>")
    async def misctools_unload(self, ctx: commands.Context, *toolsets_: str):
        """Unload one or more toolsets"""
        if not toolsets_:
            await ctx.send_help()
            return

        unloaded = []
        output = []

        for toolset in toolsets_:
            try:
                self.loader.unload(toolset)
            except UnknownToolset:
                output.append(
                    _("No such toolset with the name `{toolset}` could be found").format(
                        toolset=toolset
                    )
                )
            except NotLoaded:
                output.append(_("`{toolset}` is not currently loaded").format(toolset=toolset))
            else:
                unloaded.append(toolset)
                async with config.toolsets() as toolsets__:
                    for ts in toolsets__.copy():
                        if ts.lower() == toolset.lower():
                            toolsets__.remove(ts)

        if unloaded:
            output.append(
                _(
                    "Unloaded {count, plural, one {{count} toolset} other {{count} toolsets}}:"
                    " {sets}"
                ).format(count=len(unloaded), sets=Humanize([inline(x) for x in unloaded]))
            )

        for page in pagify("\n".join(output)):
            await ctx.send(page)
