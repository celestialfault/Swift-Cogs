import json
from pathlib import Path

from redbot.core.bot import Red

from starboard import shared
from starboard.core import Starboard
from starboard.worker import Worker

with open(str(Path(__file__).parent / "info.json")) as f:
    __red_end_user_data_statement__ = json.load(f)["end_user_data_statement"]


def setup(bot: Red):
    shared.bot = bot
    bot.loop.create_task(Worker.bootstrap())
    bot.add_cog(Starboard(bot))
