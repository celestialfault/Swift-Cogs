from typing import Union

import discord
from redbot.core import checks, commands
from redbot.core.bot import Red
from redbot.core.utils.chat_formatting import error, info, warning

from starboard.core.abc import CompositeMetaClass, Context, MixinMeta
from starboard.shared import _, tick
from starboard.worker import DEFAULT_EMOJI


async def hierarchy_allows(bot: Red, mod: discord.Member, member: discord.Member) -> bool:
    if await bot.is_owner(mod):
        return True

    guild = mod.guild
    mod_cog = bot.get_cog("Mod")
    # noinspection PyUnresolvedReferences
    mod_config = (
        (mod_cog.settings if hasattr(mod_cog, "settings") else mod_cog.config) if mod_cog else None
    )
    if mod_cog is not None and not await mod_config.guild(guild).respect_hierarchy():
        return True

    return member != guild.owner and (mod == guild.owner or mod.top_role > member.top_role)


class StarboardCommand(MixinMeta, metaclass=CompositeMetaClass):
    @commands.group(name="starboard")
    async def cmd_starboard(self, ctx: Context):
        """Manage the starboard for the current server"""

    @cmd_starboard.command(name="info")
    async def starboard_info(self, ctx: Context):
        """Get the server's current starboard settings"""
        embed = discord.Embed(
            colour=await ctx.embed_colour(),
            description=_(
                "**Starboard channel** \N{EM DASH} {channel}\n"
                "**Minimum stars required** \N{EM DASH} {min} {emoji}\n"
                "**Self-starring allowed** \N{EM DASH} {selfstar, select, y {Yes} other {No}}"
            ).format(
                channel=ctx.starboard.channel.mention
                if ctx.starboard.channel
                else _("No channel set"),
                min=ctx.starboard.min_stars,
                emoji=str(ctx.starboard.emoji),
                selfstar="y" if ctx.starboard.self_star else "n",
            ),
        ).set_author(
            name=_("Starboard settings for {guild}").format(guild=ctx.guild.name),
            icon_url=ctx.guild.icon_url,
        )
        await ctx.send(embed=embed)

    async def _ignore_sanity(self, ctx, item):
        if isinstance(item, discord.Member) and (
            not await hierarchy_allows(self.bot, ctx.author, item)
        ):
            await ctx.send(
                error(
                    _(
                        "This server's role hierarchy disallows you from taking that action."
                    ).format()
                )
            )
            return False
        elif isinstance(item, discord.TextChannel) and item == ctx.starboard.channel:
            await ctx.send(
                warning(
                    _(
                        "The starboard channel is always ignored, and cannot be manually"
                        " ignored nor unignored."
                    ).format()
                )
            )
            return False
        return True

    @cmd_starboard.command(name="ignore", aliases=["block"], usage="<member/channel>")
    @checks.mod_or_permissions(manage_messages=True)
    async def star_ignore(self, ctx: Context, item: Union[discord.TextChannel, discord.Member]):
        """Add a member or channel to the ignore list"""
        if not await self._ignore_sanity(ctx, item):
            return

        if ctx.starboard.is_ignored(item):
            await ctx.send(
                warning(
                    (
                        _("That member is already ignored.")
                        if isinstance(item, discord.Member)
                        else _("That channel is already ignored.")
                    ).format()
                )
            )
            return

        await ctx.starboard.ignore(item.id)
        await ctx.send(
            tick(
                (
                    _(
                        "{mention} will no longer have their messages or stars appear on"
                        " the starboard."
                    )
                    if isinstance(item, discord.Member)
                    else _("Messages from {mention} will no longer appear on the starboard.")
                ).format(mention=item.mention)
            ),
            allowed_mentions=discord.AllowedMentions(users=False),
        )

    @cmd_starboard.command(name="unignore", aliases=["unblock"], usage="<member|channel>")
    @checks.mod_or_permissions(manage_messages=True)
    async def star_unignore(self, ctx: Context, item: Union[discord.TextChannel, discord.Member]):
        """Remove a member or channel from the ignore list"""
        if not await self._ignore_sanity(ctx, item):
            return

        if ctx.starboard.is_ignored(item):
            await ctx.send(
                warning(
                    (
                        _("That member is not currently ignored.")
                        if isinstance(item, discord.Member)
                        else _("That channel is not currently ignored.")
                    ).format()
                )
            )
            return

        await ctx.starboard.unignore(item.id)
        await ctx.send(
            tick(
                (
                    _("{mention} will now have their messages and stars appear on the starboard.")
                    if isinstance(item, discord.Member)
                    else _("Messages from {mention} will now appear on the starboard.")
                ).format(mention=item.mention)
            ),
            allowed_mentions=discord.AllowedMentions(users=False),
        )

    @cmd_starboard.command(name="emoji")
    @checks.admin_or_permissions(manage_channels=True)
    async def starboard_emoji(self, ctx: Context, emoji: str = DEFAULT_EMOJI):
        """Set the emoji used to star messages"""
        if emoji in ("@everyone", "@here"):
            await ctx.send(_("Nice try.").format())
            return
        # Yes, this command does absolutely no checks on unicode emojis.
        # This is entirely on purpose, mainly on the account that it's just
        # not worth dealing with. If someone sets their server emoji to
        # something that isn't a valid emoji, well tough luck, it isn't my problem.
        display_as = emoji
        try:
            emoji = await commands.PartialEmojiConverter().convert(ctx, emoji)
        except commands.BadArgument:
            pass
        else:
            if not any(x for x in ctx.guild.emojis if x.id == emoji.id):
                await ctx.send(warning(_("That emoji is not local to this server.")))
                return
            display_as = str(emoji)
            emoji = emoji.id

        ctx.starboard.emoji = emoji
        await ctx.starboard.save()
        await ctx.send(info(_("Set the server's star emoji to {emoji}.").format(emoji=display_as)))

    @cmd_starboard.command(name="selfstar")
    @checks.admin_or_permissions(manage_channels=True)
    async def starboard_selfstar(self, ctx: Context, toggle: bool = None):
        """Set if members can star their own messages

        Defaults to enabled.
        """
        toggle = (not ctx.starboard.self_star) if toggle is None else toggle
        ctx.starboard.self_star = toggle
        await ctx.starboard.save()
        await ctx.send(
            tick(
                (
                    _("Members may now star their own messages.")
                    if toggle
                    else _("Members can no longer star their own messages.")
                ).format()
            )
        )

    @cmd_starboard.command(name="channel")
    @checks.admin_or_permissions(manage_channels=True)
    async def starboard_channel(self, ctx: Context, channel: discord.TextChannel = None):
        """Set the server's starboard channel"""
        if channel and channel.guild.id != ctx.guild.id:
            await ctx.send(error(_("That channel is not local to the current server.").format()))
            return

        ctx.starboard.channel = channel
        await ctx.starboard.save()
        if channel is None:
            await ctx.send(tick(_("Starboard channel cleared.").format()))
        else:
            await ctx.send(
                tick(_("Starboard channel set to {channel}.").format(channel=channel.mention))
            )

    @cmd_starboard.command(name="minstars", aliases=["stars"])
    @checks.admin_or_permissions(manage_channels=True)
    async def starboard_minstars(self, ctx: Context, stars: int):
        """Set the minimum stars required to send a message to the starboard"""
        if stars < 1:
            await ctx.send(warning(_("The minimum star count must be at least 1.").format()))
            return

        if stars > len([x for x in ctx.guild.members if not x.bot]):
            await ctx.send(
                warning(
                    _(
                        "There's not enough members in the server to reach that minimum star count."
                    ).format()
                )
            )
            return

        ctx.starboard.min_stars = stars
        await ctx.starboard.save()
        await ctx.tick()
