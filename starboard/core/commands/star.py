from typing import Dict, Sequence

import discord
from redbot.core import checks, commands
from redbot.core.utils.chat_formatting import error, info, inline, warning

from starboard.checks import starboard_is_setup
from starboard.core.abc import CompositeMetaClass, Context, MixinMeta
from starboard.exceptions import IgnoredChannelException, SelfStarException, StarboardException
from starboard.message import StarboardMessage
from starboard.shared import _, tick
from starboard.stats import gen_statistics


def index(seq: Sequence, item) -> str:
    if isinstance(seq, dict):
        seq = list(seq.values())
    index_ = str(seq.index(item) + 1)
    return index_.zfill(len(index_))


class StarCommand(MixinMeta, metaclass=CompositeMetaClass):
    @commands.group(invoke_without_command=True, aliases=["stars"])
    @checks.bot_has_permissions(add_reactions=True)
    @starboard_is_setup()
    async def star(self, ctx: Context, message: StarboardMessage):
        """Star a message by its ID"""
        if message.has_starred(ctx.author):
            await ctx.send(
                warning(
                    _(
                        "You've already starred that message. *(are you trying to remove it?"
                        " try `{prefix}star remove {id}`)*"
                    ).format(prefix=ctx.clean_prefix, id=message.message.id)
                ),
                delete_after=15,
            )
            return

        try:
            message.add_star(ctx.author)
        except IgnoredChannelException:
            if not ctx.starboard.channel.is_nsfw() and message.channel.is_nsfw():
                await ctx.send(
                    error(
                        _(
                            "Cannot star a message in a NSFW channel if the starboard channel"
                            " is not similarly marked NSFW."
                        ).format()
                    )
                )
            else:
                await ctx.send(
                    warning(
                        _(
                            "That message is in a channel the server admins have marked as ignored."
                        ).format()
                    )
                )
        except SelfStarException:
            await ctx.send(warning(_("You cannot star your own messages.").format()))
        except StarboardException:
            await ctx.send(warning(_("Failed to add star.").format()))
        else:
            await ctx.tick()

    @star.command(name="remove", aliases=["rm"])
    async def star_remove(self, ctx: Context, message: StarboardMessage):
        """Remove a previously added star"""
        if not message.has_starred(ctx.author):
            await ctx.send(
                warning(_("You haven't starred that message yet.").format()), delete_after=15
            )
            return

        try:
            message.remove_star(ctx.author)
        except StarboardException:
            await ctx.send(warning(_("Failed to remove your star for that message.").format()))
        else:
            await ctx.tick()

    @star.command(name="show")
    @checks.bot_has_permissions(embed_links=True)
    async def star_show(self, ctx: Context, message: StarboardMessage):
        """Show the starboard message for the message given"""
        if not message.stars or message.hidden:
            raise commands.BadArgument
        await ctx.send(**await message.contents())

    @star.command(name="stats")
    async def star_stats(self, ctx: Context, *, member: discord.Member = None):
        """View Starboard statistics for yourself or another member"""
        member = member or ctx.author
        data = await gen_statistics(ctx.guild, member)
        await ctx.send(
            info(
                _(
                    "{member} has:\n\n"
                    "**\N{BULLET}** Recieved \N{WHITE MEDIUM STAR} **{received, plural,"
                    " one {# star}  other {# stars}}** with a record of \N{WHITE MEDIUM STAR}"
                    " **{max_received,  plural, one {# star} other {# stars}}** on a single"
                    " message\n"
                    "**\N{BULLET}** Given \N{WHITE MEDIUM STAR} **{given, plural, one {# star}"
                    " other {# stars}}**\n"
                    "**\N{BULLET}** Had **{sb_messages, plural, one {# message}"
                    " other {# messages}}** sent to the starboard channel"
                ).format(
                    member=member.mention,
                    **{k: v.get(member, 0) for k, v in data.items() if isinstance(v, dict)},
                )
            ),
            allowed_mentions=discord.AllowedMentions(users=False),
        )

    @star.command(name="leaderboard", aliases=["lb"])
    @commands.cooldown(1, 20, commands.BucketType.guild)
    @checks.bot_has_permissions(embed_links=True)
    async def star_leaderboard(self, ctx: Context):
        """Starboard leaderboards for the current server"""
        data = await gen_statistics(ctx.guild, top=4)
        medals = [
            "\N{FIRST PLACE MEDAL}",
            "\N{SECOND PLACE MEDAL}",
            "\N{THIRD PLACE MEDAL}",
            "**`{}.`**",
        ]

        def fmt_data(dct: Dict[discord.Member, int], emoji: str):
            items = []
            keys = list(dct.keys())
            for k, v in dct.items():
                medal = medals[min(len(medals) - 1, keys.index(k))].format(index(keys, k))
                items.append(
                    f"{medal} {getattr(k, 'mention', inline(str(k)))} \N{EM DASH} **{v:,}** {emoji}"
                    f"".strip()
                )
            return "\n".join(items) or _(
                "*No stats to display! Star something by reacting to a message with {emoji}!*"
            ).format(emoji=str(ctx.starboard.emoji))

        embed = discord.Embed(colour=await ctx.embed_colour())

        embed.set_author(
            name=_("Leaderboard for {guild}").format(guild=ctx.guild.name),
            icon_url=ctx.guild.icon_url,
        )
        embed.add_field(
            name=_("Stars Given").format(),
            value=fmt_data(data["given"], ctx.starboard.emoji),
            inline=False,
        )
        embed.add_field(
            name=_("Stars Received").format(),
            value=fmt_data(data["received"], ctx.starboard.emoji),
            inline=False,
        )
        embed.add_field(
            name=_("Starboard Messages").format(),
            value=fmt_data(data["sb_messages"], ""),
            inline=False,
        )
        embed.set_footer(
            text=_(
                "{stars, plural, one {# star has} other {# stars have}} been given"
                " in total across {messages, plural, one {# message} other {# messages}}"
            ).format(stars=data["total_given"], messages=data["total_messages"])
        )

        await ctx.send(embed=embed)

    @star.command(name="hide")
    @checks.mod_or_permissions(manage_messages=True)
    async def star_hide(self, ctx: Context, message: StarboardMessage):
        """Hide a message from the starboard channel"""
        if message.hidden:
            return await ctx.send(
                error(_("That message is already hidden from the starboard.").format())
            )
        message.hidden = True
        message.update()
        await ctx.send(
            tick(
                _("Okay, the message sent by {author} is now hidden.").format(
                    author=getattr(message.author, "mention", _("*an unknown user*"))
                )
            ),
            allowed_mentions=discord.AllowedMentions(users=False),
        )

    @star.command(name="unhide")
    @checks.mod_or_permissions(manage_messages=True)
    async def star_unhide(self, ctx: Context, message: StarboardMessage):
        """Unhide a message from the starboard channel"""
        if message.hidden is False:
            return await ctx.send(
                error(_("That message is not currently hidden from the starboard.").format())
            )
        message.hidden = False
        message.update()
        await ctx.send(
            tick(
                _("Okay, the message sent by {author} is no longer hidden.").format(
                    author=getattr(message.author, "mention", _("*an unknown user*"))
                )
            ),
            allowed_mentions=discord.AllowedMentions(users=False),
        )

    @star.command(name="update", usage="<message>...")
    @checks.mod_or_permissions(manage_messages=True)
    async def star_update(self, ctx: Context, *messages: StarboardMessage):
        """Queue a message for an update"""
        if not messages:
            return await ctx.send_help()
        for message in messages:
            await message.update_cached_message()
            message.update()
        await ctx.send(
            tick(
                _(
                    "{count, plural, one {1 message has} other"
                    " {{count} messages have}} been queued for an update."
                ).format(count=len(messages))
            )
        )

    @star.command(name="ignore", aliases=["unignore", "block", "unblock"], usage="", hidden=True)
    @checks.mod_or_permissions(manage_messages=True)
    async def _ignore_moved(self, ctx: commands.Context, *__):
        """Moved to `[p]starboard (un)ignore`"""
        await ctx.send(
            _("This command has been moved to the `{prefix}starboard` command group.").format(
                prefix=ctx.clean_prefix
            )
        )
