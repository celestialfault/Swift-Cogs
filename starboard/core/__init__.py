from redbot.core import commands as _red_commands
from redbot.core.bot import Red as _Red

from red_icu import cog_i18n as _cog_i18n
from starboard.core import abc
from starboard.core.commands import Commands
from starboard.core.events import Events
from starboard.shared import _


@_cog_i18n(_)
class Starboard(Commands, Events, _red_commands.Cog, metaclass=abc.CompositeMetaClass):
    """Star quality messages so they'll never be forgotten to the depths of #general"""

    def __init__(self, bot: _Red):
        super().__init__()
        self.bot = bot
