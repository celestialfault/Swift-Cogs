import textwrap
from datetime import datetime
from typing import Iterable, List, Optional

import discord
from discord.ext import commands
from redbot.core.commands import Context

from starboard.db import Database
from starboard.exceptions import (
    BlockedAuthorException,
    BlockedUserException,
    IgnoredChannelException,
    SelfStarException,
    StarException,
)
from starboard.shared import _, log

__all__ = ("StarboardMessage", "resolve_starred_by")


def resolve_starred_by(data: dict) -> List[int]:
    """Resolve the user IDs of who starred from a message's raw data set"""
    # boy I sure do love backwards compatibility
    return data.get("starred_by") or data.get("starrers") or data.get("members", [])


class StarboardMessage(commands.Converter):
    """Starboard message class

    Parameters
    ----------
    message: :class:`discord.Message`
        The message object to use
    starboard: :class:`StarboardGuild`
        The starboard associated to the guild that this message originated in
    """

    __slots__ = ("message", "cfg", "starboard_message", "starred_by", "last_update", "_hidden")

    def __init__(self, **kwargs):
        from starboard.worker import WorkerGuild

        # these are optional for the purpose of being able to use a d.py Converter
        # instead of manually calling ctx.starboard.get_message() in every command
        self.message: discord.Message = kwargs.get("message")
        self.cfg: WorkerGuild = kwargs.get("guild")
        self.starboard_message: Optional[discord.Message] = None
        self.starred_by: List[int] = []
        self.last_update = datetime.utcnow()
        self.hidden: bool = False

    def __repr__(self):
        return (
            f"<StarboardMessage stars={self.stars} hidden={self.hidden} message={self.message!r}>"
        )

    @property
    def as_dict(self) -> dict:
        """Get the current message data as a :class:`dict`"""
        return {
            "channel_id": self.channel.id,
            "author_id": self.author.id,
            "starred_by": self.starred_by,
            "starboard_message": getattr(self.starboard_message, "id", None),
            "hidden": self.hidden,
        }

    @property
    def guild(self):
        return self.message.guild

    @property
    def worker(self):
        return self.cfg.worker

    # pylint:disable=arguments-differ

    @classmethod
    async def convert(cls, ctx: Context, argument: str, **kwargs) -> "StarboardMessage":
        """"""  # this docstring is intentionally empty
        from starboard.worker import Worker

        if not ctx.guild:
            raise commands.NoPrivateMessage
        worker = Worker.get_worker(ctx.guild)
        guild = worker.guilds[ctx.guild]

        try:
            argument = int(argument)
        except ValueError:
            raise commands.BadArgument()

        if not guild.channel:
            raise commands.BadArgument(
                _(
                    "This server does not have a starboard channel setup; ask your server"
                    " admins to set it with `{prefix}starboard channel`."
                ).format(prefix=ctx.clean_prefix)
            )

        message = await guild.get_message(message_id=argument, channel=ctx.channel, **kwargs)
        if message is None:
            raise commands.BadArgument(_("The given message could not be found.").format())
        if not message.valid:
            raise commands.BadArgument()

        return message

    # pylint:enable=arguments-differ

    async def setup_from_data(self, data: dict) -> None:
        self.starred_by = resolve_starred_by(data)
        self.hidden = data.get("hidden", False)

        if data.get("starboard_message") is not None:
            channel = self.cfg.channel
            if channel is None:
                raise RuntimeError(
                    "Starboard channel is None, but we're somehow loading message data"
                )

            try:
                mid = data.get("starboard_message")
                self.starboard_message = await channel.fetch_message(mid)
            except discord.NotFound:
                self.starboard_message = None

    async def save(self) -> None:
        """Save the current message data to Config"""
        log.debug("Saving data for message %s", self.message.id)
        await Database.save_message(self.message.guild, self.message.id, self.as_dict)
        self.last_update = datetime.utcnow()

    #################################
    #   Message data

    @property
    def colour(self) -> int:
        """Embed colour based on star count

        This is `shamelessly ripped`_ from R.Danny, mainly because I cannot be bothered
        to make my own version of this.

        .. _shamelessly ripped:
            https://github.com/Rapptz/RoboDanny/blob/2a9ea96/cogs/stars.py#L151-L166
        """
        p = min(self.stars / 20.0, 1.0)

        red = 255
        green = int((194 * p) + (253 * (1 - p)))
        blue = int((12 * p) + (247 * (1 - p)))
        return (red << 16) + (green << 8) + blue

    @property
    def author(self) -> discord.Member:
        """Convenience alias for :attr:`discord.Message.author`"""
        return self.message.author

    @property
    def channel(self) -> discord.TextChannel:
        """Convenience alias for :attr:`discord.Message.channel`"""
        return self.message.channel

    @property
    def id(self) -> int:
        """Convenience alias for :attr:`discord.Message.id`"""
        return self.message.id

    @property
    def attachments(self) -> Iterable[str]:
        """Get all attachments for the current message"""
        attachs = [
            *[x for x in self.message.attachments if x.height or x.width],
            *[x for x in self.message.embeds if x.thumbnail or x.image],
        ]

        for attach in attachs:
            if isinstance(attach, discord.Attachment):
                yield attach.url
            elif isinstance(attach, discord.Embed):
                if attach.image:
                    yield attach.image.url
                if attach.thumbnail:
                    yield attach.thumbnail.url

    @property
    def attachment_url(self) -> Optional[str]:
        """Get the first available attachment"""
        return next(iter(self.attachments), discord.Embed.Empty)

    async def contents(self) -> Optional[dict]:
        """Get the starboard message contents for this message.

        The returned :class:`dict` can be passed directly
        to :func:`discord.TextChannel.send`.
        """
        if not self.valid:
            return None

        # descriptions can be 2048 characters, but with Nitro message character limits are planned
        # be increased to 4000 characters, so ensure we don't encounter 400s while trying to send
        # messages to the starboard by ensuring this limit is maintained
        content = (
            textwrap.shorten(self.message.content, 2000, placeholder="\N{HORIZONTAL ELLIPSIS}")
            if self.message.content
            else discord.Embed.Empty
        )
        emoji = str(self.cfg.emoji)
        embed = discord.Embed(
            colour=self.colour, timestamp=self.message.created_at, description=content
        ).set_author(
            name=self.author.display_name, icon_url=self.author.avatar_url_as(format="png")
        )

        if self.attachment_url:
            embed.set_image(url=self.attachment_url)

        embed.add_field(
            name="\N{ZERO WIDTH JOINER}",
            value=_("**[Jump to original message]({jump})**").format(jump=self.message.jump_url),
        )

        return {
            "content": (
                f"{emoji} {self.stars} \N{EM DASH} {self.channel.mention} ({self.message.id})"
            ),
            "embed": embed,
        }

    @property
    def valid(self) -> bool:
        if not self.message:
            return False
        return bool(self.message.content or [*self.message.embeds, *self.message.attachments])

    async def update_cached_message(self) -> None:
        """Update the currently cached Discord message"""
        if cached := discord.utils.get(shared.bot.cached_messages, id=self.message.id):
            self.message = cached
        else:
            self.message = await self.channel.fetch_message(self.message.id)

    #################################
    #   Starboard message management

    def update(self) -> None:
        """Queue the message for an update"""
        if self in self.worker.queue:
            return
        self.last_update = datetime.utcnow()
        self.worker.queue.put_nowait(self)

    async def edit_message(self) -> None:
        if self.cfg.channel is None:
            return

        if self.stars >= self.cfg.min_stars and not self.hidden and self.valid:
            if self.starboard_message is not None:
                try:
                    await self.starboard_message.edit(**await self.contents())
                except discord.NotFound:
                    self.starboard_message = None
                    return await self.edit_message()
            else:
                try:
                    self.starboard_message = await self.cfg.channel.send(**await self.contents())
                except discord.Forbidden:
                    pass
        else:
            if self.starboard_message is not None:
                try:
                    await self.starboard_message.delete()
                except (discord.HTTPException, AttributeError):
                    pass
                finally:
                    self.starboard_message = None

        await self.save()

    #################################
    #   Message stars

    @property
    def stars(self) -> int:
        """The amount of stars the current message has"""
        return len(self.starred_by)

    def has_starred(self, member: discord.Member) -> bool:
        """Check if ``member`` has starred this message"""
        return member.id in self.starred_by

    def _check_star(
        self, member: discord.Member, *, check_selfstar: bool = False, removing: bool = False
    ) -> None:
        # prevent messages in nsfw channels from being starred if the starboard channel
        # isn't marked as nsfw, since not doing this may very well end up having the potential to
        # bring up community guidelines related issues
        if self.channel.is_nsfw() and not self.cfg.channel.is_nsfw():
            raise IgnoredChannelException()

        star_check = self.has_starred(member)
        if removing is True:
            star_check = not star_check

        if not self.valid or star_check:
            raise StarException()
        if self.cfg.is_ignored(self.author):
            raise BlockedAuthorException()
        if self.cfg.is_ignored(member) or member.bot:
            raise BlockedUserException()
        if self.cfg.is_ignored(self.channel):
            raise IgnoredChannelException()

        if check_selfstar and member == self.author and not self.cfg.self_star:
            raise SelfStarException()

    def add_star(self, member: discord.Member) -> None:
        """Add a star

        Parameters
        ----------
        member: :class:`discord.Member`
            The member to add a star for

        Raises
        ------
        StarException
            Raised if the message is invalid or if ``member`` has already starred the message
        BlockedAuthorException
            Raised if the message author is blocked from the associated guild's starboard
        BlockedUserException
            Raised if ``member`` is blocked from the associated guild's starboard
        IgnoredChannelException
            Raised if the channel the message is in is ignored in the associated guild
        SelfStarException
            Raised if the associated guild is set to not allow self-stars
            and the message author is the same as ``member``.
        """
        self._check_star(member, check_selfstar=True)
        self.starred_by.append(member.id)
        self.update()

    def remove_star(self, member: discord.Member) -> None:
        """Remove a star

        Parameters
        ----------
        member: :class:`discord.Member`
            The member to remove a star for

        Raises
        ------
        StarException
            Raised if ``member`` has not already starred the message
        BlockedAuthorException
            Raised if the message author is blocked from the associated guild's starboard
        BlockedException
            Raised if ``member`` is blocked from the associated guild's starboard
        IgnoredChannelException
            Raised if the channel the message is in is ignored in the associated guild
        """
        self._check_star(member, removing=True)
        self.starred_by.remove(member.id)
        self.update()
