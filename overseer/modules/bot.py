import traceback

import discord
from redbot.core import Config, commands  # pylint:disable=unused-import
from redbot.core.utils.chat_formatting import box, pagify

from overseer.core import event, Module, translate as translate_

translate = translate_.group("bot")


class Bot(
    Module,
    id="bot",
    name=translate.lazy("__name__"),
    description=translate.lazy("__description__"),
    scope=Config.GLOBAL,
):
    def icon_uri(self, member: discord.Member = None):
        return getattr(member, "avatar_url_as", self.bot.user.avatar_url_as)(format="png")

    async def can_modify_settings(self, member: discord.Member):
        return await self.bot.is_owner(member)

    @classmethod
    async def from_ctx(cls, ctx: commands.Context):
        i = cls()
        return i if await i.can_modify_settings(ctx.author) else None

    @event("errors", description=translate.lazy("events.command_errors"))
    def command_error(self, ctx: commands.Context, error: Exception):
        err_embeds = [
            discord.Embed(colour=discord.Colour.red(), description=box(pg, lang="py"))
            for pg in pagify("".join(traceback.format_tb(error.__traceback__)), shorten_by=20)
        ]
        yield {
            "content": translate("command_exception", qualified=ctx.command.qualified_name),
            "embed": err_embeds[0],
        }
        if len(err_embeds) > 1:
            yield from err_embeds[1:]

    @event("startup", description=translate.lazy("events.startup"))
    def startup(self):
        return translate("started")
