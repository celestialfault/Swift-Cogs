from datetime import timedelta
from typing import List, Tuple

import discord
from redbot.core.utils.chat_formatting import escape

from overseer.core import ghost, event, Module, translate as translate_, AuditLogHandler
from swift_i18n import Humanize

translate = translate_.group("guild")


class Guild(
    Module,
    id="server",
    name=translate.lazy("__name__"),
    description=translate.lazy("__description__"),
):
    audit = AuditLogHandler(discord.AuditLogAction.guild_update, (lambda *_: True))
    update = ghost()

    @update.event("2fa", description=translate.lazy("events.2fa"))
    async def _update_2fa(self, before: discord.Guild, after: discord.Guild):
        if any([before.unavailable, after.unavailable]):
            return

        if before.mfa_level != after.mfa_level:
            responsible = await self.audit()
            return translate(
                ("mfa_enabled" if after.mfa_level == 1 else "mfa_disabled")
                + ("" if not responsible else "_responsible"),
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("owner", description=translate.lazy("events.owner"))
    def _update_owner(self, before: discord.Guild, after: discord.Guild):
        if any([before.unavailable, after.unavailable]):
            return

        if before.owner_id != after.owner_id:
            return translate(
                "ownership_transferred",
                before=before.owner.mention,
                after=after.owner.mention,
                before_id=before.owner.id,
                after_id=after.owner.id,
            )

    @update.event("name", description=translate.lazy("events.name"))
    async def _update_name(self, before: discord.Guild, after: discord.Guild):
        if any([before.unavailable, after.unavailable]):
            return

        if before.name != after.name:
            responsible = await self.audit()
            return translate(
                "server_name" if not responsible else "server_name_responsible",
                after=escape(after.name, formatting=True),
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("afk")
    def _update_afk(self):
        pass

    @_update_afk.event("channel", description=translate.lazy("events.afk.channel"))
    async def _afk_channel(self, before: discord.Guild, after: discord.Guild):
        if any([before.unavailable, after.unavailable]):
            return

        if before.afk_channel != after.afk_channel:
            responsible = await self.audit()
            return translate(
                ("afk_channel_changed" if after.afk_channel else "afk_channel_removed")
                + ("" if not responsible else "_responsible"),
                channel=getattr(after.afk_channel, "mention", None),
                id=getattr(after.afk_channel, "id", None),
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @_update_afk.event("timeout", description=translate.lazy("events.afk.timeout"))
    async def _afk_timeout(self, before: discord.Guild, after: discord.Guild):
        if any([before.unavailable, after.unavailable]):
            return

        if before.afk_timeout != after.afk_timeout:
            responsible = await self.audit()
            return translate(
                "afk_timeout" + ("" if not responsible else "_responsible"),
                after=Humanize(timedelta(seconds=after.afk_timeout)),
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("filter", description=translate.lazy("events.filter"))
    async def _update_content_filter(self, before: discord.Guild, after: discord.Guild):
        if any([before.unavailable, after.unavailable]):
            return None

        if before.explicit_content_filter != after.explicit_content_filter:
            responsible = await self.audit()
            return translate(
                "content_filter" + ("_responsible" if responsible else ""),
                status=after.explicit_content_filter.name,
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("region", description=translate.lazy("events.region"))
    async def _update_region(self, before: discord.Guild, after: discord.Guild):
        if any([before.unavailable, after.unavailable]):
            return None

        if before.region != after.region:
            responsible = await self.audit()
            return translate(
                "region_changed" + ("" if not responsible else "_responsible"),
                after=translate("voice_regions", str(after.region), default=str(after.region)),
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @update.event("verification", description=translate.lazy("events.verification"))
    async def _update_verification(self, before: discord.Guild, after: discord.Guild):
        if any([before.unavailable, after.unavailable]):
            return None

        if before.verification_level != after.verification_level:
            responsible = await self.audit()
            levels = translate("verification_levels")
            return translate(
                "verification_level_set" + ("" if not responsible else "_responsible"),
                after=levels[after.verification_level],
                moderator=responsible.mention,
                mod_id=responsible.id,
            )

    @event("emoji", description=translate.lazy("events.emoji"))
    def emoji(self, before: List[discord.Emoji], after: List[discord.Emoji]):
        changed: List[Tuple[str, dict]] = [
            *[("emoji_added", {"emoji": str(x), "name": x.name}) for x in after if x not in before],
            *[("emoji_removed", {"emoji": x.name}) for x in before if x not in after],
            *[
                (
                    "emoji_renamed",
                    {
                        "emoji": str(x),
                        "before": discord.utils.get(before, id=x.id).name,
                        "after": x.name,
                    },
                )
                for x in after
                if x in before and discord.utils.get(before, id=x.id).name != x.name
            ],
        ]

        # We're specifically excluding audit log information from this event due
        # to the fact that this event can also be used in an the context of an
        # emoji-changelog channel, which would likely not benefit from such information.
        if not changed:
            return
        for tid, args in changed:
            yield translate(tid, **args)
