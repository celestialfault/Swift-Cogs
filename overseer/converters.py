from typing import Tuple, Dict, Union, Optional

import discord
from redbot.core import commands
from redbot.core.utils.chat_formatting import escape

from overseer.core import Logger, translate, Module
from overseer.core.module import modules
from overseer.utils import flatten, unflatten_dict


def status_diff(
    before: Dict[str, Union[bool, int]], after: Dict[str, Union[bool, int]]
) -> Dict[str, Union[bool, int]]:
    before = flatten(before)
    after = flatten(after)
    changed = {}
    for k, v in after.items():
        if k not in before and v is False:
            continue
        if before.get(k) != v:
            changed[k] = v
    return unflatten_dict(changed)


class ModuleConverter(commands.Converter):
    async def convert(self, ctx, argument):
        mod = modules.get(argument.lower())
        if mod:
            mod = await mod.from_ctx(ctx)
        if mod is None:
            raise commands.BadArgument(
                translate(
                    "no_such_module", module=escape(argument, mass_mentions=True, formatting=True)
                )
            )
        return mod


class SettingsConverter(commands.Converter):
    async def convert(
        self, ctx: commands.Context, argument: str
    ) -> Tuple[str, Tuple[Logger, Union[None, bool, discord.TextChannel]]]:
        argument = argument.split("=")
        name = argument.pop(0).replace(":", ".").split(".")
        if argument:
            new_val = "=".join(argument)
            if new_val.lower() in ("true", "1", "yes", "on"):
                new_val = True
            elif new_val.lower() in ("false", "0", "no", "off"):
                new_val = False
            else:
                try:
                    new_val = await commands.TextChannelConverter().convert(ctx, new_val)
                except commands.BadArgument:
                    raise commands.BadArgument(
                        translate(
                            "channel_bool_convert_fail",
                            item=escape(new_val, mass_mentions=True, formatting=True),
                        )
                    )
        else:
            new_val = None

        modn = name.pop(0)
        try:
            module = discord.utils.find(
                lambda x: x[0].lower() == modn.lower(), list(modules.items())
            )[1]
        except (IndexError, TypeError):
            module: Optional[Module] = None
        else:
            module: Optional[Module] = await module.from_ctx(ctx)

        if not module:
            raise commands.BadArgument(
                translate(
                    "no_such_module", module=escape(modn, mass_mentions=True, formatting=True)
                )
            )

        if not name:
            raise commands.BadArgument(translate("expected_setting"))
        logger: Logger = discord.utils.get(
            [*module.loggers, *module.groups], key=".".join(name).lower()
        )

        if not logger or not await logger.passes_checks(module):
            raise commands.BadArgument(
                translate(
                    "invalid_setting_key",
                    module=module.id,
                    event=escape(".".join(name), mass_mentions=True, formatting=True),
                )
            )

        return module.id, (logger, new_val)
