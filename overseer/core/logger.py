from __future__ import annotations

import warnings

from contextvars import ContextVar, copy_context
from functools import wraps
from types import FunctionType
from typing import (
    Callable,
    AsyncIterable,
    Generator,
    Union,
    List,
    Awaitable,
    Optional,
    Iterable,
    Tuple,
    MutableMapping,
    Any,
)

import discord

from swift_i18n.util import LazyStr
from overseer.core.shared import log

__all__ = ("event", "Event", "logger", "Logger", "ghost", "check")
# While these aren't listed in __all__, they can be loosely considered as part of the Overseer API,
# as they can still prove to be very useful in other situations outside of how it's used in
# the core Overseer cog.
ARGUMENTS: ContextVar[Tuple[tuple, dict]] = ContextVar("arguments", default=((), {}))
INSTANCE = ContextVar("instance")


def flatten_dict(dct: MutableMapping[str, Any], parent_key: str = "", sep: str = "."):
    # originally from https://stackoverflow.com/a/6027615
    for k, v in dct.items():
        new_key = parent_key + sep + str(k) if parent_key else str(k)
        if isinstance(v, MutableMapping):
            yield from flatten_dict(v, new_key, sep=sep)
        else:
            yield (new_key, v)


def check(predicate: Callable):
    """Attach a check to an event

    If the predicate given returns a non-truthy value, this event will not be listed in
    [p]overseer, and will not be configurable.

    .. important::
        If your event is already enabled, the event parser will still be invoked when called,
        even if this check later returns a non-truthy value.

    Example
    --------
    .. code-block:: python

        @event("event")
        @check(lambda module: ...)
        def event(self):
            pass

    Parameters
    -----------
    predicate: Callable[[Event, Optional[tuple], Optional[dict]], Union[bool, Awaitable[bool]]]
        The check predicate to call
    """

    def wrapper(event_: Union[Callable, Event]):
        if isinstance(event_, Event):
            event_.checks.append(predicate)
        else:
            if hasattr(event_, "__checks__"):
                event_.__checks__.append(predicate)
            else:
                event_.__checks__ = [predicate]
        return event_

    return wrapper


def event(
    key: str = None, *, default: bool = False, description: Union[str, LazyStr] = None
) -> Callable[[Callable], Event]:
    """Create a new :class:`Event`

    Keyword Args
    ------------
    key: :class:`str`
        The setting ID for the event that users can use to enable or disable logging
        through ``[p]overseer``. If this isn't specified, this is determined from
        the decorated function's name.
    default: :class:`bool`
        The default state of this event. Defaults to :obj:`False`; you should only set this to
        :obj:`True` if you know what this implies.
    description: Union[:class:`str`, :class:`~swift_i18n.util.LazyStr`, :obj:`None`]
        The user-friendly description for this :class:`Event`. This is displayed in
        ``[p]overseer``.
    """
    if key in ("_log_channel",):
        raise ValueError(f"{key} is a reserved config key")
    if key.startswith("_"):
        warnings.warn("Your event's config key should not start with an underscore", UserWarning)

    return lambda f: Event(
        f, key=key if key is not None else f.__name__, default=default, description=description
    )


def logger(*args, **kwargs):
    warnings.warn("logger() is deprecated; use event() instead", DeprecationWarning)
    return event(*args, **kwargs)


def ghost(*_) -> Event:
    """Create a ghost :class:`Event` group

    The created :class:`Event` will not show up in ``[p]overseer``, but can be used
    as a pseudo-group event, such as to allow for bundling top-level settings
    into a single update event call.

    Some usages of this in the core Overseer module set can be found in the Server and Voice
    modules.

    All positional arguments passed to this function are discarded, and as such this may be used
    either as a standard function or as a method decorator.

    Example
    -------

    .. code-block:: python

        class Example(Module, ...):
            update = ghost()

            # this is shown to users in [p]overseer as if it was created with a plain `@event()`
            # decorator, meaning that this shows up as just 'example', and not 'update.example'
            @update.event("example", description="Example usage of ghost events")
            def example(self, *args, **kwargs):
                return "Hello world"
    """
    return Event(None, key="")


class Event:
    """Core logging handler

    You shouldn't create this class directly, but instead use :func:`event` as a method decorator.

    .. important::
        Attempting to retrieve an Event from an instantiated :class:`~overseer.core.module.Module`
        instance will instead return a partial function that can be called to log an event.

    .. important::
        When calling your event parser, the following keyword arguments are reserved for internal
        Overseer use:

        - ``__dest``
        - ``__skip_settings_check``

    Attributes
    ----------
    parent: Optional[:class:`Event`]
        The parent for this :class:`Event`
    id: :class:`str`
        The internal ID for this event. If this is empty, this event is considered
        a ghost group, and will never log anything directly, even if ``children``
        is an empty list.
    description: Optional[Union[str, LazyStr]]
        The description provided for this event
    children: List[:class:`Event`]
        All children event parsers this event groups together
    checks: List[:class:`Callable`]
        All checks this event uses to determine if its settings can be modified.
    """

    def __init__(
        self,
        func: Optional[Callable],
        *,
        key: str,
        default: bool = False,
        description: Union[str, LazyStr] = None,
        parent: Event = None,
    ):
        if func is None and key != "":
            raise ValueError("The given function is not set but an event key is specified")

        self._func = func
        self.parent = parent
        self.id = key
        self._default = default
        self.description = description
        self.children: List[Event] = []
        # noinspection PyUnresolvedReferences
        self.checks: List[Callable[["Module"], Union[bool, Awaitable[bool]]]] = getattr(
            func, "__checks__", []
        )

    def __hash__(self):
        return hash(self.key)

    def __repr__(self):
        return (
            f"<{type(self).__name__} id={self.id!r} key={self.key!r} default={self.default!r}"
            f" func={self._func!r}>"
        )

    def __str__(self):
        return self.key

    def __get__(self, instance, owner) -> Union[Event, Callable[..., Awaitable[None]]]:
        if not instance:
            return self

        @wraps(self.log)
        async def wrapper(*a, **kw):
            async def wrap():
                ARGUMENTS.set((a, kw))
                INSTANCE.set(instance)
                await self.log(*a, **kw)

            return await copy_context().run(wrap)

        return wrapper

    @property
    def key(self) -> str:
        """The full internal ID for this event including the keys for any and all parent events"""
        return ".".join([*[x.key for x in self.all_parents], self.id])

    @property
    def default(self):
        return {v.id: v.default for v in self.children} if self.children else self._default

    @property
    def all_parents(self) -> List[Event]:
        """Get a list of all parents for this event

        Returns
        --------
        List[Event]
            The list of all parents to this event. The list is ordered with the root event
            at the beginning, and with the parent to this event at the end
            (i.e ``[root, root.child, root.child.child, ...]``). This event is
            not included.
        """
        if not self.parent:
            return []
        return [x for x in [*self.parent.all_parents, self.parent] if x.key != ""]

    async def passes_checks(self, instance) -> bool:
        """Check if this event would be visible in [p]overseer output"""
        for check_ in self.checks:
            if not await discord.utils.maybe_coroutine(check_, instance):
                return False
        if self.parent and not await self.parent.passes_checks(instance):
            return False
        return True

    def walk_children(self) -> Iterable[Event]:
        """Walk through all registered child event parsers"""
        for child in self.children:
            yield child
            if child.children:
                yield from child.walk_children()

    async def log_destination(self):
        """Get the configured log output channel; this may differ from the module's log channel"""
        # Log destinations don't make much sense for parent or ghost events, since they never
        # directly log anything.
        if self.children or self.id == "":
            return None

        instance = INSTANCE.get()
        status = await instance.conf.get_raw(*self.key.split("."), default=self.default)
        if not status:
            return None
        if not isinstance(status, bool):
            return instance.bot.get_channel(status)
        return await instance.log_destination()

    @staticmethod
    async def send(item, dest):
        if not item or not dest:
            return

        try:
            kwargs = (
                item
                if isinstance(item, dict)
                else {"embed" if isinstance(item, discord.Embed) else "content": item}
            )
            # never allow any mentions regardless of what the event handler wants
            kwargs["allowed_mentions"] = discord.AllowedMentions(
                everyone=False, roles=False, users=False
            )
            await dest.send(**kwargs)
        except discord.HTTPException as e:
            if e.status in (400,):
                raise

            if isinstance(e, discord.Forbidden):
                log.warning(
                    "Encountered forbidden error while sending a log"
                    " message to channel #%s (%s)",
                    dest,
                    dest.id,
                )

    async def _group_call(
        self,
        args: tuple,
        kwargs: dict,
        settings: dict,
        root_dest: Optional[discord.TextChannel] = ...,
    ):
        if not self.children:
            raise RuntimeError("Group log call attempted on an Event without children")
        instance = INSTANCE.get()
        if root_dest is ...:
            root_dest = await instance.log_destination()

        # If none of our child loggers are enabled, don't bother continuing any further
        if not any(settings.get(x.key) for x in self.walk_children()):
            return

        for child in self.children:
            if child.children:
                await copy_context().run(
                    child._group_call,  # pylint:disable=protected-access
                    args,
                    kwargs,
                    settings,
                    root_dest,
                )
                continue

            status = settings.get(child.key, None)
            if not status:
                continue

            try:
                await copy_context().run(
                    child.log,
                    *args,
                    **kwargs,
                    __skip_settings_check=True,
                    __dest=instance.bot.get_channel(status) or root_dest,
                )
            except Exception as e:  # pylint:disable=broad-except
                log.exception("Failed to call event parser %r", child, exc_info=e)

    async def log(self, *args, **kwargs):
        """Internal method to log output from the event parser.

        A partial function to this method is returned when retrieving an Event from an
        instantiated Module class.

        Raises
        ------
        RuntimeError
            Raised if this event is used after the given module was unloaded. You should override
            the :func:`~overseer.core.module.Module.unregister` class method to properly
            handle this.
        """
        instance = INSTANCE.get()
        if getattr(instance, "_Module__unloaded", True):
            raise RuntimeError(
                "Either Overseer has been unloaded or reloaded since this module was created, or"
                " the given module was unregistered (you should override"
                " Module.unregister to handle this)"
            )

        if self.children:
            await self._group_call(args, kwargs, dict(flatten_dict(await instance.conf.all())))
            return

        if self.id == "":
            raise RuntimeError(
                "Attempted to invoke a ghost event - did you forget to attach"
                " your sub-events as children to this event?"
            )

        __dest = kwargs.pop("__dest", None)
        if not kwargs.pop("__skip_settings_check", False):
            enabled = await instance.conf.get_raw(*self.key.split("."), default=False)
            if not enabled:
                return

        dest: discord.TextChannel = __dest if __dest else await self.log_destination()
        if (
            not dest
            or not dest.permissions_for(dest.guild.get_member(instance.bot.user.id)).send_messages
        ):
            return

        ret = await discord.utils.maybe_coroutine(self._func, instance, *args, **kwargs)
        if not isinstance(ret, (AsyncIterable, Generator, list, tuple, set)):
            ret = [ret]
        if isinstance(ret, AsyncIterable):
            ret = [x async for x in ret]

        for i in ret:
            if not i:
                continue
            await self.send(i, dest)

    def event(
        self, key: str, *, default: bool = False, description: Union[str, LazyStr] = None
    ) -> Callable[[FunctionType], "Event"]:
        # noinspection PyUnresolvedReferences
        """Attach a new event to the current event as a child

        By attaching a child event, this event will no longer log directly,
        but will instead call all child events as if you had called them
        manually with the same arguments.

        Example
        -------

        >>> from overseer.core import Module
        >>> class MyMod(Module, ...):  # see docs on Module for applicable class kwargs
        ...     @event("parent")
        ...     def parent(self):
        ...         # this function will never be called, and as such the arguments
        ...         # this method accepts doesn't affect anything.
        ...         pass
        ...
        ...     @parent.event("child")
        ...     def child(self, arg1, arg2):
        ...         # this function *will* be called from calling `parent`,
        ...         # and it's return value logged
        ...         return "Hello world"
        >>> await MyMod().parent(..., ...)  # this is equivalent to MyMod().child(..., ...)

        Calling a parent event will not call the parent function, but will instead
        call all child events (in this case, just ``child``).

        Additionally, ``[p]overseer`` will show your child event's setting as ``parent.child``.
        """

        def decorator(func):
            child = Event(func, key=key, default=default, description=description, parent=self)
            self.children.append(child)
            return child

        return decorator

    def logger(self, *args, **kwargs):
        warnings.warn("Event.logger is deprecated; use Event.event instead", DeprecationWarning)
        return self.event(*args, **kwargs)


class Logger(Event):
    """"""  # Intentionally left empty.

    def __init__(self, *args, **kwargs):
        warnings.warn("Logger is deprecated; use Event instead", DeprecationWarning)
        super().__init__(*args, **kwargs)
