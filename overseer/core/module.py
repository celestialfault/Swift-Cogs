import re
from datetime import timedelta, datetime
from functools import partial
from typing import Optional, Union, ClassVar, List, Dict, Type, Callable, Tuple, Awaitable

import discord
from discord.abc import Snowflake
from redbot.core import Config, commands
from redbot.core.bot import Red

# noinspection PyProtectedMember
from redbot.core.config import Group

from overseer.core.logger import Event
from overseer.core.shared import config, translate, rebuild_defaults, log
from overseer.utils import replace_dict_values, flatten
from swift_i18n.util import LazyStr

# noinspection PyTypeChecker
bot: Red = None
__all__ = ("Module", "register", "unregister", "modules")
modules: Dict[str, Type["Module"]] = {}
ID_VALID = re.compile(r"[a-z]{2,}")
DEFAULT_SCOPES = (
    Config.GLOBAL,
    Config.GUILD,
    Config.MEMBER,
    Config.USER,
    Config.ROLE,
    Config.CHANNEL,
)


def cfg_scope(cfg: Config, scope: str, *identifiers):
    """Safely retrieve a base config Group with unknown scopes

    This works around default scopes not being able to be retrieved with
    :attr:`redbot.core.config.Config.custom` as of Red 3.1.
    """
    if scope in DEFAULT_SCOPES:
        if scope == Config.GLOBAL:
            return cfg
        elif scope == Config.CHANNEL:
            return cfg.channel(*identifiers)
        else:
            return getattr(cfg, scope.lower())(*identifiers)
    return cfg.custom(
        scope, *[getattr(x, "id") if isinstance(x, Snowflake) else x for x in identifiers]
    )


def register(module: Type["Module"]):
    """Register a module for use

    You should only need to use this if you're loading modules manually,
    such as if you passed ``dont_register`` as a class kwarg.

    If the given module was marked as disabled when it was created, this will raise
    a :class:`RuntimeError`.
    """
    if module.id in modules:
        old = modules.pop(module.id)
        old.unregister()
        old._Module__unloaded = True  # pylint:disable=protected-access
    module.register()
    modules[module.id] = module
    rebuild_defaults()


def unregister(module: Union[Type["Module"], str], *, rebuild: bool = True):
    """Unload a module from usage

    Keyword Args
    ------------
    rebuild: bool
        If this is :obj:`False`, config defaults will not be rebuilt. You should only need
        to use this if you're manually reloading a module, or if you're doing very weird things.
    """
    mod = modules.pop(getattr(module, "id", module))
    mod._Module__unloaded = True  # pylint:disable=protected-access
    if rebuild:
        rebuild_defaults()
    try:
        mod.unregister()
    except Exception as e:  # pylint:disable=broad-except
        log.exception("Failed to call unregister hook for module %r", mod, exc_info=e)


def add_descriptions(items: List[str], descriptions: Dict[str, str] = None) -> str:
    if descriptions is None:
        descriptions = {}
    for item in items:
        index = items.index(item)
        items[index] = "**{}** \N{EM DASH} {}".format(
            item, descriptions.get(item, translate("no_description"))
        )
    return "\n".join(items)


def startup(red: Red):
    global bot  # pylint:disable=global-statement
    bot = red


def teardown():
    for name, module in list(modules.items()):
        module._Module__unloaded = True  # pylint:disable=protected-access
        unregister(name, rebuild=False)


class Module:
    """Base logging module class

    Subclasses should pass the following class kwargs:

    Keyword Args
    ------------
    id: :class:`str`
        The internal module ID. This is also used in ``[p]overseer`` commands to
        identify your module. This must contain only lowercase ``a-z`` characters, and
        be at least two characters long.
    name: Union[:class:`str`, :class:`~swift_i18n.util.LazyStr`]
        The user-friendly name to display for your module. This is used in ``[p]overseer``.
    description: Optional[Union[:class:`str`, :class:`~swift_i18n.util.LazyStr`]]
        An optional description to use for your module in ``[p]overseer``.
        This should be relatively short.
    scope: Optional[:class:`str`]
        The config scope for your module. Defaults to ``GUILD``. Default scopes require identifiers
        to be the same as the associated built-in Config scope method parameters; this means the
        member scope requires a :class:`discord.Member` to be passed as the sole identifier.
    dont_register: Optional[:class:`bool`]
        If this is a truthy value, your module will not be registered automatically. This
        is implicitly implied if your module is marked as disabled.

    Example
    -------
    >>> class MyMod(
    >>>     Module, id='mymod', name='My Module', description='A short description of my module'
    >>> ):
    >>>     ..
    """

    __slots__ = ("_identifiers",)

    id: ClassVar[str]
    name: ClassVar[Union[str, LazyStr]]
    loggers: ClassVar[List[Event]]
    groups: ClassVar[List[Event]]
    description: ClassVar[Union[None, str, LazyStr]] = None
    config_scope: ClassVar[str] = Config.GUILD

    # Internal flag; used to complain if this module is used after the attached Overseer
    # cog instance or this module has since been unloaded and/or reloaded.
    __unloaded: ClassVar[bool] = False

    @property
    def bot(self):
        return bot

    def __init_subclass__(cls, **kwargs):
        cls.id = kwargs.pop("id")
        if not ID_VALID.fullmatch(cls.id):
            raise ValueError(f"Module ID {kwargs['id']!r} is not valid")
        cls.name = kwargs.pop("name")
        cls.description = kwargs.pop("description", None)
        cls.config_scope = kwargs.pop("scope", Config.GUILD)
        seen = []
        # noinspection PyTypeChecker
        loggers = [
            x for x in map(partial(getattr, cls), dir(cls)) if isinstance(x, Event) and x.key != ""
        ]
        for logger in loggers:
            if logger.key not in seen:
                seen.append(logger.key)
                continue
            raise RuntimeError(f"An event with the key {logger.key!r} was created more than once")
        cls.loggers = [x for x in loggers if not x.children]
        cls.groups = [x for x in loggers if x.children]

        skip_registration = kwargs.pop("dont_register", False)
        if kwargs:
            raise TypeError(f"Unexpected class kwarg {next(iter(kwargs.keys()))}")
        if not skip_registration:
            register(cls)

    def __init__(self, *identifiers):
        self._identifiers = identifiers

    def __str__(self):
        return self.id

    def __repr__(self):
        return f"<{self.__class__.__qualname__} id={self.id!r} friendly={str(self.name)!r}>"

    def __hash__(self):
        return hash((self.id, *self._identifiers))

    @classmethod
    async def from_ctx(cls, ctx: commands.Context) -> Optional["Module"]:
        """Retrieve a module from a :class:`~redbot.core.commands.Context` object

        .. important::
            If the given context author cannot modify the current module's settings,
            this **must** return :obj:`None`; otherwise, users can bypass
            setting management restrictions for this module.
        """
        if not ctx.guild:
            raise commands.NoPrivateMessage

        i = cls(ctx.guild)
        return i if await i.can_modify_settings(ctx.author) else None

    @classmethod
    def register(cls):
        """Helper method called on module init.

        Can be overridden by subclasses to implement their own setup routine
        """
        pass

    @classmethod
    def unregister(cls):
        """Helper method called when performing unload hooks.

        Can be overridden by subclasses to implement their own cleanup routine
        """
        pass

    @property
    def conf(self) -> Optional[Group]:
        """Retrieve the current module config group"""
        return cfg_scope(config, self.config_scope, *self._identifiers).get_attr(self.id)

    @property
    def guild(self) -> Optional[discord.Guild]:
        return next(iter(x for x in self._identifiers if isinstance(x, discord.Guild)), None)

    async def can_modify_settings(self, member: discord.Member) -> bool:
        return member.guild.owner == member or member.guild_permissions.administrator

    async def log_destination(self) -> Optional[discord.TextChannel]:
        """Retrieve the log channel that should be used for logging the current module"""
        return self.bot.get_channel(await self.conf.get_raw("_log_channel", default=None))

    async def set_events(self, *opts: Tuple[Event, Union[None, bool, discord.TextChannel]]):
        """Manage this module's logger settings

        Parameters
        ----------
        *opts: Tuple[Event, Union[None, bool, discord.TextChannel]]
            Tuples correlating to the Event to update and the new bool status to update them to.
            If the second item in the tuple is :obj:`None`, the current value is toggled from
            their current state. If the second item is instead a :class:`discord.TextChannel`
            object, it's treated as a channel to override the current module's log destination
            with.
        """
        # In normal use, this will very likely never actually be raised, but still a half-decent
        # idea just in case someone's being dumb.
        if self.__unloaded:
            raise RuntimeError(
                "Overseer has been unloaded or reloaded since this module was created"
            )

        for logger, new_val in opts:
            keys = logger.key.split(".")
            cval = await self.conf.get_raw(*keys, default=logger.default)

            if isinstance(cval, dict):
                new_val = replace_dict_values(
                    cval,
                    getattr(new_val, "id", new_val)
                    if new_val is not None
                    else not all(flatten(cval).values()),
                )
            else:
                new_val = getattr(new_val, "id", new_val) if new_val is not None else not cval

            await self.conf.set_raw(*keys, value=new_val)

    async def responsible_moderator(
        self,
        action: Union[discord.AuditLogAction, List[discord.AuditLogAction]],
        check: Union[Callable[[discord.AuditLogEntry], Union[bool, Awaitable[bool]]], partial],
        limit: int = 3,
        **kwargs,
    ) -> Optional[discord.Member]:
        """Get the responsible moderator for an action from audit logs

        Only audit log entries from the last 30 seconds are considered.

        .. important::
            This method queries the Discord API on every invocation. You should use
            :class:`AuditLogHandler` in event parsers instead of directly calling this method.

        .. seealso::
            * :class:`overseer.core.audit.AuditLogHandler`
        """
        if (
            not self.guild
            or not self.guild.get_member(self.bot.user.id).guild_permissions.view_audit_log
            or not await config.guild(self.guild).get_raw("_responsible_mod", default=False)
        ):
            return None

        async for audit in self.guild.audit_logs(
            limit=limit, action=action if not isinstance(action, list) else None, **kwargs
        ):
            audit: discord.AuditLogEntry = audit
            if isinstance(action, list) and audit.action not in action:
                continue
            if audit.created_at + timedelta(seconds=30) < datetime.utcnow():
                continue
            if await discord.utils.maybe_coroutine(check, audit):
                return audit.user
